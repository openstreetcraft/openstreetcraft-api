// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import net.openstreetcraft.coordinates.BlockLocation;

public class MinecraftPointTest {

  @Test
  public void convertsBlockLocationIntoPoint() {
    BlockLocation location = BlockLocation.of(1.0, 2.0);
    Point actual = new MinecraftPoint(location);
    assertEquals(1, actual.getX(), Double.MIN_VALUE);
    assertEquals(2, actual.getY(), Double.MIN_VALUE);
  }

  @Test
  public void convertsPointIntoBlockLocation() {
    BlockLocation location = BlockLocation.of(1.0, 2.0);
    MinecraftPoint actual = new MinecraftPoint(location);
    assertEquals(location, actual.toBlockLocation());
  }

}
