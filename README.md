[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.ixilon/openstreetcraft-api/badge.svg)](https://maven-badges.herokuapp.com/maven-central/de.ixilon/openstreetcraft-api)

# Overview

Facade to the Openstreetcraft REST APIs as Java client library.

Clients using this service classes don't have to worry about either the domain complexity
or the technical complexity of the backend data integrations.

# Usage

The [JAR file](http://search.maven.org/#search%7Cga%7C1%7Cg:%22de.ixilon%22%20AND%20a:%22openstreetcraft-api%22)
is available on Maven central.

Online documentation can be found [here](https://openstreetcraft.gitlab.io/openstreetcraft-api/docs/javadoc/index.html).

## RestTemplate

Every service requires a specialized
[RestTemplate](https://docs.spring.io/spring/docs/current/javadoc-api/org/springframework/web/client/RestTemplate.html)
which connects to Openstreetcraft API gateway endpoint.

```
RestTemplateFactory restTemplateFactory = new RestTemplateFactory();
RestTemplate restTemplate = restTemplateFactory.getRestTemplate();
```

Creates a RestTemplate instance which is preconfigured connecting to Openstreetcraft production instance.
This setup can be changed with the following environment variables:

* `OPENSTREETCRAFT_PROTOCOL`
* `OPENSTREETCRAFT_HOST`
* `OPENSTREETCRAFT_PORT`
* `OPENSTREETCRAFT_USERNAME`
* `OPENSTREETCRAFT_PASSWORD`

## PartitionService

Many services require a `Partition` parameter.

A partition is an area in the real world where the difference between the highest and lowest altitude is smaller than the limits of the Minecraft world. This information can be used to divide the Minecraft map into several sub-maps.

```
TileMap tileMap = ...
TileService tileService = ...
PartitionService partitionService = new PartitionService(tileMap, tileService);
```

See [here](https://gitlab.com/openstreetcraft/tms#usage) for how to create `TileMap` and `TileService`.

Search for a partition at a specific location on the map:

```
Projection projection = new ProjectionBuilder(World.EARTH).build();
ProjectedLocation projectedLocation = projection.transform(sphericalLocation);
Partition partition = partitionService.findPartitionAtLocation(projectedLocation);
```

## CoordinatesService

```
CoordinatesService coordinatesService = new CoordinatesService(restTemplate);
```

Convert Minecraft (x,z) coordinate to spherical (lat,lon) coordinate:

```
SphericalLocation sphericalLocation = coordinatesService.getSphericalLocation(blockLocation, partition);
```

Convert spherical (lat,lon) coordinate to Minecraft (x,z) coordinate:

```
BlockLocation blockLocation = coordinatesService.getMinecraftLocation(sphericalLocation, partition);
```

## LocationService

```
LocationService locationService = new LocationService(restTemplate);
```

Transforming a postal address description to a location on the Earth's surface:

```
SphericalLocation sphericalLocation = locationService.getLocation(address);
```

Transforming a location on the Earth's surface to a postal address description:

```
String address = locationService.getAddress(sphericalLocation);
```

## MapService

```
MapService mapService = new MapService(restTemplate);
```

List supported world maps

```
Set<World> worlds = mapService.getWorlds();
```

List supported layers on a world map

```
Set<Layer> layers = mapService.getLayers(world);
```

Query samples on a layer

```
RectangularBoundingBox bbox = new RectangularBoundingBox.Builder()
  .withLowerLeft(new SphericalPoint(minSphericalLocation))
  .withUpperRight(new SphericalPoint(maxSphericalLocation))
  .build();

Raster<Byte> samples = mapService.getByteSamples(world, layer, bbox, width, height);
byte value = samples.getSample(x, y);
```

or

```
Raster<Float> samples = mapService.getFloatSamples(world, layer, bbox, width, height);
float value = samples.getSample(x, y);
```

The type of the samples (byte or float) depends on the layer:
Most layers are of type byte, but elevation layers are of type float.

### Biomes

Convert bytes into biomes with the help of the [BiomeConverter](https://openstreetcraft.gitlab.io/openstreetcraft-api/docs/javadoc/net/openstreetcraft/minecraft/biome/BiomeConverter.html).

```
Raster<Biome> samples = mapService
  .getByteSamples(world, Layer.BIOME, bbox, width, height)
  .convert(new BiomeConverter());
```

Use [BiomeParser](https://openstreetcraft.gitlab.io/openstreetcraft-api/docs/javadoc/net/openstreetcraft/minecraft/biome/BiomeParser.html) to cast generic biome into Java- or Bedrock-edition specific biome.

```
BiomeParser<BedrockBiome> parser = new BiomeParser<>(BedrockBiome.class);
BedrockBiome bedrockBiome = parser.identifiable(Biome.OCEAN);
```

## OverpassService

```
int cacheSize = 9; // optimal value
OverpassService overpassService = new CachingOverpassService(restTemplate, cacheSize);
```

Get OpenStreetMap structures inside an area on the Earth's surface:

```
Osm osm = overpassService.getOsm(bbox);
```

The osm package provides classes to [decode OpenStreetMap structures](https://openstreetcraft.gitlab.io/openstreetcraft-api/docs/javadoc/net/openstreetcraft/osm/util/package-summary.html) and to [perform geometric operations](https://openstreetcraft.gitlab.io/openstreetcraft-api/docs/javadoc/net/openstreetcraft/osm/geom/package-summary.html) on ways and nodes.

## TimezoneService

```
TimezoneService timezoneService = new TimezoneService(restTemplate);
```

Get timezone information at a location on the Earth's surface:

```
Timezone timezone = timezoneService.getTimezone(sphericalLocation);
```

## WeatherService

```
WeatherService weatherService = new WeatherService(restTemplate);
```

Get weather conditions at a location on the Earth's surface:

```
Weather weather = weatherService.getWeather(sphericalLocation);
```

# Integration test

The integration test requires running Openstreetcraft REST APIs.

You have to [register on Google Maps](https://developers.google.com/maps/documentation/javascript/get-api-key)
and set the environment variable `GOOGLE_MAPS_GEOCODING_API_KEY`. 

```
docker login registry.gitlab.com
docker-compose up
``` 

will start the Openstreetcraft application.
After the application has been booted verify that all services are up and running with the
[discovery service dashboard](http://localhost:8761).

The integration test can be started now:

```
./gradlew integrationTest
```
