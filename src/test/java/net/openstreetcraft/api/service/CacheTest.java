// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.Test;

public class CacheTest {

  @Test
  public void removesEldestEntry() {
    Map<String, String> cache = new Cache<>(2);

    cache.put("foo", "1");
    cache.put("bar", "2");
    cache.put("baz", "3");

    assertFalse(cache.containsKey("foo"));
    assertTrue(cache.containsKey("bar"));
    assertTrue(cache.containsKey("baz"));
  }

  @Test
  public void renewsUsedEntry() {
    Map<String, String> cache = new Cache<>(2);

    cache.put("foo", "1");
    cache.put("bar", "2");
    
    assertNotNull(cache.get("foo"));
    
    cache.put("baz", "3");

    assertTrue(cache.containsKey("foo"));
    assertFalse(cache.containsKey("bar"));
    assertTrue(cache.containsKey("baz"));
  }

}
