// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.util;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

public final class Filter {

  private Filter() {
    // static utility class
  }
  
  /**
   * Creates a new list containing all elements that match the predicate.
   * Elements can be cast to type T.  
   */
  public static <T, P> Iterable<T> filter(Iterable<P> list, Predicate<P> predicate) {
    ImmutableList.Builder<T> result = ImmutableList.builder();
    for (P obj : list) {
      if (predicate.apply(obj)) {
        result.add((T) obj);
      }
    }
    return result.build();
  }

}
