// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.junit.Before;
import org.junit.Test;

import de.ixilon.osm.schema.ObjectFactory;
import de.ixilon.osm.schema.Osm;
import de.ixilon.osm.schema.OsmAdapter;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmRoot;
import de.ixilon.osm.schema.OsmWay;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Unmarshaller;
import net.openstreetcraft.osm.util.OsmUtils;

public class OsmPolygonTest {

  private OsmPolygon osmPolygon;

  @Before
  public void setUp() throws Exception {
    JAXBContext context = JAXBContextFactory.createContext(new Class[] {ObjectFactory.class}, null);
    Unmarshaller unmarshaller = context.createUnmarshaller();
    try (InputStream resourceAsStream = getClass().getResourceAsStream("osm.xml")) {
      XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
      XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(resourceAsStream);
      @SuppressWarnings("unchecked")
      JAXBElement<OsmRoot> response =
          (JAXBElement<OsmRoot>) unmarshaller.unmarshal(xmlStreamReader);
      Osm osm = new OsmAdapter(response.getValue());
      for (OsmWay way : OsmUtils.getWays(osm)) {
        Iterable<OsmNode> nodes = OsmUtils.getNodes(osm);
        osmPolygon = new OsmPolygon(way, OsmNodeList.copyOf(nodes));
      }
    }
  }

  @Test
  public void ignoresPolygonOutsideOfBoundingBox() {
    Rectangle bbox = Rectangle.builder().xmin(10.0).ymin(47.0).xmax(11.0).ymax(48.0).build();
    assertFalse(osmPolygon.isInside(bbox));
  }

  @Test
  public void findsPolygonInsideOfEnclosingBoundingBox() {
    Rectangle bbox = Rectangle.builder().xmin(11.0).ymin(48.0).xmax(12.0).ymax(49.0).build();
    assertTrue(osmPolygon.isInside(bbox));
  }

  @Test
  public void findsPolygonInsideOfCrossingBoundingBox() {
    Rectangle bbox =
        Rectangle.builder().xmin(11.4348).ymin(48.2650).xmax(11.4349).ymax(48.2651).build();
    assertTrue(osmPolygon.isInside(bbox));
  }

  @Test
  public void findsPolygonInsideOfContainingBoundingBox() {
    Rectangle bbox =
        Rectangle.builder().xmin(11.4348).ymin(48.26495).xmax(11.43485).ymax(48.265).build();
    assertTrue(osmPolygon.isInside(bbox));
  }

}
