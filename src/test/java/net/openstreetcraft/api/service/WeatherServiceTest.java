// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import org.apache.http.HttpHost;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.api.model.Precipitation;
import net.openstreetcraft.api.model.Weather;
import net.openstreetcraft.projection.SphericalLocation;

public class WeatherServiceTest {

  private static final HttpHost GATEWAY = new HttpHost("gateway");
  private static final String ENDPOINT = "http://gateway/weather";

  private MockRestServiceServer mockServer;
  private WeatherService service;

  @Before
  public void setUp() {
    RestTemplate restTemplate = new RestTemplate();

    mockServer = MockRestServiceServer.createServer(restTemplate);
    service = new WeatherService(restTemplate, GATEWAY);
  }

  @Test
  public void getsWeatherAtLocation() {
    mockServer.expect(requestTo(ENDPOINT + "/weather?longitude=3.4&latitude=1.2"))
        .andRespond(withSuccess("{\"temperature\": 1, \"precipitation\": \"RAIN\"}",
            MediaType.APPLICATION_JSON));

    Weather actual = service.getWeather(SphericalLocation.of(3.4, 1.2));

    mockServer.verify();
    assertEquals(1, actual.getTemperature().get().intValue());
    assertEquals(Precipitation.RAIN, actual.getPrecipitation().get());
  }

}
