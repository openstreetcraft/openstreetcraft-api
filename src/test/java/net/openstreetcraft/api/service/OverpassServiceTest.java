// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.esri.core.geometry.Envelope2D;

public class OverpassServiceTest {

  @Test
  public void roundsBoundingBox() {
    Envelope2D original = new Envelope2D(48.2649, 11.4347, 48.2659, 11.4357);
    Envelope2D expected = new Envelope2D(48.2630, 11.4330, 48.2670, 11.4370);

    Envelope2D actual = OverpassService.round(original);

    assertEquals(expected.xmin, actual.xmin, Double.MIN_VALUE);
    assertEquals(expected.xmax, actual.xmax, Double.MIN_VALUE);
    assertEquals(expected.ymin, actual.ymin, Double.MIN_VALUE);
    assertEquals(expected.ymax, actual.ymax, Double.MIN_VALUE);
  }

  @Test
  public void formatsQueryString() {
    Envelope2D original = new Envelope2D(48.2649, 11.4347, 48.2659, 11.4357);
    String expected = "*[bbox=48.2649,11.4347,48.2659,11.4357]";

    String actual = OverpassService.query(original);

    assertEquals(expected, actual);
  }

}
