// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import java.util.Objects;

import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.model.Layer;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.minecraft.world.World;
import net.openstreetcraft.projection.SphericalLocation;

@Service
public class CachingMapService extends MapService {

  private final Cache<Key, Raster<Float>> floatSamplesCache;
  private final Cache<Key, Raster<Byte>> byteSamplesCache;

  /**
   * Caching results of map service.  
   */
  @Autowired
  public CachingMapService(RestTemplate restTemplate, HttpHost gateway,
      @Value("${map.cache:1}") int cacheSize) {
    super(restTemplate, gateway);
    this.floatSamplesCache = new Cache<>(cacheSize);
    this.byteSamplesCache = new Cache<>(cacheSize);
  }

  @Override
  public synchronized Raster<Float> getFloatSamples(World world, Layer layer,
      BoundingBox<SphericalLocation> bbox, int width, int height) throws RestClientException {
    return floatSamplesCache.computeIfAbsent(new Key(world, layer, bbox, width, height),
        key -> super.getFloatSamples(key.world, key.layer, key.bbox, key.width, key.height));
  }

  @Override
  public synchronized Raster<Byte> getByteSamples(World world, Layer layer,
      BoundingBox<SphericalLocation> bbox, int width, int height) throws RestClientException {
    return byteSamplesCache.computeIfAbsent(new Key(world, layer, bbox, width, height),
        key -> super.getByteSamples(key.world, key.layer, key.bbox, key.width, key.height));
  }

  private static class Key {
    private final World world;
    private final Layer layer;
    private final BoundingBox<SphericalLocation> bbox;
    private final int width;
    private final int height;

    Key(World world, Layer layer, BoundingBox<SphericalLocation> bbox, int width, int height) {
      this.world = world;
      this.layer = layer;
      this.bbox = bbox;
      this.width = width;
      this.height = height;
    }

    @Override
    public int hashCode() {
      return Objects.hash(world, layer, bbox, width, height);
    }

    @Override
    public boolean equals(Object obj) {
      if (!(obj instanceof Key)) {
        return false;
      }

      Key other = (Key) obj;

      return Objects.equals(this.world, other.world)
          && Objects.equals(this.layer, other.layer)
          && Objects.equals(this.bbox, other.bbox)
          && Objects.equals(this.width, other.width)
          && Objects.equals(this.height, other.height);
    }
  }
}
