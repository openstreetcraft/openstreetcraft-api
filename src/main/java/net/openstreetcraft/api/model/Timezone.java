// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.model;

import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Timezone {

  private ZonedDateTime currentTime;
  private ZonedDateTime sunrise;
  private ZonedDateTime sunset;

  /**
   * Construct a Timezone instance.
   */
  public Timezone(@JsonProperty("currentTime") String currentTime,
      @JsonProperty("sunrise") String sunrise,
      @JsonProperty("sunset") String sunset)
        throws DateTimeParseException {
    this.currentTime = ZonedDateTime.parse(currentTime);
    this.sunrise = ZonedDateTime.parse(sunrise);
    this.sunset = ZonedDateTime.parse(sunset);
  }

  @Override
  public int hashCode() {
    return Objects.hash(currentTime, sunrise, sunset);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Timezone)) {
      return false;
    }

    Timezone other = (Timezone) obj;
    return Objects.equals(this.currentTime, other.currentTime)
        && Objects.equals(this.sunrise, other.sunrise)
        && Objects.equals(this.sunset, other.sunset);
  }

  @Override
  public String toString() {
    return "Timezone [currentTime=" + currentTime + ", sunrise=" + sunrise + ", sunset=" + sunset
        + "]";
  }

  /**
   * Returns the current time. 
   */
  public ZonedDateTime getCurrentTime() {
    return currentTime;
  }

  /**
   * Returns the sunrise time. 
   */
  public ZonedDateTime getSunrise() {
    return sunrise;
  }

  /**
   * Returns the sunset time. 
   */
  public ZonedDateTime getSunset() {
    return sunset;
  }

}
