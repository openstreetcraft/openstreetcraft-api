// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.model;

import java.util.Objects;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Weather {

  private Optional<Integer> temperature;
  private Optional<Precipitation> precipitation;

  /**
   * Constructs a Weather instance.
   */
  public Weather(@JsonProperty("temperature") Integer temperature,
      @JsonProperty("precipitation") Precipitation precipitation) {
    this.temperature = Optional.ofNullable(temperature);
    this.precipitation = Optional.ofNullable(precipitation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(temperature, precipitation);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Weather)) {
      return false;
    }

    Weather other = (Weather) obj;
    return Objects.equals(this.temperature, other.temperature)
        && Objects.equals(this.precipitation, other.precipitation);
  }

  @Override
  public String toString() {
    return "Weather [temperature=" + temperature + ", precipitation=" + precipitation + "]";
  }

  /**
   * Returns the current temperature in degree Celsius.
   */
  public Optional<Integer> getTemperature() {
    return temperature;
  }

  /**
   * Returns the current precipitation (rain, snow, thunder).
   */
  public Optional<Precipitation> getPrecipitation() {
    return precipitation;
  }
}
