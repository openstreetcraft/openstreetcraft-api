// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import org.apache.http.HttpHost;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.projection.SphericalLocation;

public class LocationServiceTest {

  private static final HttpHost GATEWAY = new HttpHost("gateway");
  private static final String ENDPOINT = "http://gateway/location";

  private MockRestServiceServer mockServer;
  private LocationService service;

  @Before
  public void setUp() {
    RestTemplate restTemplate = new RestTemplate();

    mockServer = MockRestServiceServer.createServer(restTemplate);
    service = new LocationService(restTemplate, GATEWAY);
  }

  @Test
  public void getsAddressOfLocation() {
    mockServer.expect(requestTo(ENDPOINT + "/address?longitude=3.4&latitude=1.2"))
        .andRespond(withSuccess("foo", MediaType.TEXT_PLAIN));

    String actual = service.getAddress(SphericalLocation.of(3.4, 1.2));

    mockServer.verify();
    assertEquals("foo", actual);
  }

  @Test
  public void getsLocationOfAddress() {
    mockServer.expect(requestTo(ENDPOINT + "/location?address=foo")).andRespond(
        withSuccess("{\"latitude\": 1.2, \"longitude\": 3.4}", MediaType.APPLICATION_JSON));

    SphericalLocation actual = service.getLocation("foo");

    mockServer.verify();
    assertEquals(SphericalLocation.of(3.4, 1.2), actual);
  }

  @Test
  public void getsLocationOfAddressNearbyCurrentLocation() {
    mockServer.expect(
        requestTo(ENDPOINT + "/location?address=foo&longitude=7.80000&latitude=5.60000"))
        .andRespond(
            withSuccess("{\"latitude\": 1.2, \"longitude\": 3.4}", MediaType.APPLICATION_JSON));

    SphericalLocation actual = service.getLocation("foo", 
        SphericalLocation.of(7.800001, 5.600001));

    mockServer.verify();
    assertEquals(SphericalLocation.of(3.4, 1.2), actual);
  }

}
