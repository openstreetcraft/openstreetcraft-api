// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.model;

import java.util.Locale;

/**
 * Factory to create a layer descriptor.
 */
public enum Layer {

  BIOME,
  COMPRESSED,
  COVER,
  ELEVATION,
  PLASTIC_C1,
  PLASTIC_C2,
  PLASTIC_C3,
  PLASTIC_C4;

  /**
   * Create a layer by its case-insensitive name. 
   */
  public static Layer of(String name) {
    for (Layer value : values()) {
      if (value.toString().equals(name.toUpperCase(Locale.ENGLISH))) {
        return value;
      }
    }
    throw new IllegalArgumentException(name);
  }

}
