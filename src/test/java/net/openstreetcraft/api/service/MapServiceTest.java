// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import java.nio.ByteBuffer;

import org.apache.http.HttpHost;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.ImmutableSet;

import net.openstreetcraft.api.geom.Point;
import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.geom.RectangularBoundingBox;
import net.openstreetcraft.api.model.Layer;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.minecraft.world.World;
import net.openstreetcraft.projection.SphericalLocation;

public class MapServiceTest {

  private static final HttpHost GATEWAY = new HttpHost("gateway");
  private static final String ENDPOINT = "http://gateway/map";

  private MockRestServiceServer mockServer;
  private MapService service;

  @Before
  public void setUp() {
    RestTemplate restTemplate = new RestTemplate();

    mockServer = MockRestServiceServer.createServer(restTemplate);
    service = new MapService(restTemplate, GATEWAY);
  }

  @Test
  public void listsWorlds() {
    mockServer.expect(requestTo(ENDPOINT + "/maps"))
        .andRespond(withSuccess("[\"EARTH\"]", MediaType.APPLICATION_JSON));

    ImmutableSet<World> actual = service.getWorlds();
    ImmutableSet<World> expected = ImmutableSet.of(World.EARTH);

    // mockServer.verify();
    assertEquals(expected, actual);
  }

  @Test
  public void listsLayers() {
    mockServer.expect(requestTo(ENDPOINT + "/maps/EARTH/layers"))
        .andRespond(withSuccess("[\"BIOME\", \"ELEVATION\"]", MediaType.APPLICATION_JSON));

    ImmutableSet<Layer> actual = service.getLayers(World.EARTH);
    ImmutableSet<Layer> expected = ImmutableSet.of(Layer.BIOME, Layer.ELEVATION);

    mockServer.verify();
    assertEquals(expected, actual);
  }

  @Test
  public void getsByteSamples() {
    byte[] samples = new byte[] {1, 2, 3, 4};

    mockServer
        .expect(requestTo(ENDPOINT + "/maps/EARTH/layers/BIOME/bytes?"
            + "minx=5.0&miny=6.54321&maxx=7.0&maxy=8.0&width=2&height=2"))
        .andRespond(withSuccess(samples, MediaType.APPLICATION_OCTET_STREAM));

    BoundingBox<SphericalLocation> bbox = BoundingBox.of(
        SphericalLocation.of(6.54321, 7),
        SphericalLocation.of(8, 5));

    Raster<Byte> actual = service.getByteSamples(World.EARTH, Layer.BIOME, bbox, 2, 2);

    mockServer.verify();

    assertEquals(1, (byte) actual.getSample(0, 0));
    assertEquals(2, (byte) actual.getSample(1, 0));
    assertEquals(3, (byte) actual.getSample(0, 1));
    assertEquals(4, (byte) actual.getSample(1, 1));
  }

  @Test
  public void getsFloatSamples() {
    byte[] samples = new byte[4 * Float.BYTES];

    System.arraycopy(float2ByteArray(1.2f), 0, samples, 0 * Float.BYTES, Float.BYTES);
    System.arraycopy(float2ByteArray(2.3f), 0, samples, 1 * Float.BYTES, Float.BYTES);
    System.arraycopy(float2ByteArray(3.4f), 0, samples, 2 * Float.BYTES, Float.BYTES);
    System.arraycopy(float2ByteArray(4.5f), 0, samples, 3 * Float.BYTES, Float.BYTES);

    mockServer
        .expect(requestTo(ENDPOINT + "/maps/EARTH/layers/ELEVATION/floats?"
            + "minx=5.0&miny=6.54321&maxx=7.0&maxy=8.0&width=2&height=2"))
        .andRespond(withSuccess(samples, MediaType.APPLICATION_OCTET_STREAM));

    BoundingBox<SphericalLocation> bbox = BoundingBox.of(
        SphericalLocation.of(6.54321, 7),
        SphericalLocation.of(8, 5));

    Raster<Float> actual = service.getFloatSamples(World.EARTH, Layer.ELEVATION, bbox, 2, 2);

    mockServer.verify();

    assertEquals(1.2f, (float) actual.getSample(0, 0), Float.MIN_VALUE);
    assertEquals(2.3f, (float) actual.getSample(1, 0), Float.MIN_VALUE);
    assertEquals(3.4f, (float) actual.getSample(0, 1), Float.MIN_VALUE);
    assertEquals(4.5f, (float) actual.getSample(1, 1), Float.MIN_VALUE);
  }

  private static byte[] float2ByteArray(float value) {
    return ByteBuffer.allocate(4).putFloat(value).array();
  }
}
