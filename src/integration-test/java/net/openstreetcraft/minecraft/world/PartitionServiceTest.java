// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.world;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import net.openstreetcraft.coordinates.BlockLocation;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.projection.ProjectedLocation;
import net.openstreetcraft.tms.model.Tile;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {PartitionService.class, TileServiceConfiguration.class})
public class PartitionServiceTest {

  @Autowired
  PartitionService service;
  
  @Test
  public void findPartitionAtLocation() throws IOException {
    // EPSG:4326 WGS 84 lon=13.3813403, lat=54.4125288
    // ESRI:54001 Plate Carree x=1489604, y=6057175
    ProjectedLocation location = ProjectedLocation.of(1489604, 6057175);

    Partition actualPartition = service.findPartitionAtLocation(location).get();
    Partition expectedPartition = new Partition(
        new Tile(656, 490, 11),
        BoundingBox.of(
            ProjectedLocation.of(1458299.6572000012, 6070333.828600001),
            ProjectedLocation.of(1491067.6572000012, 6037565.828600001)),
        -77.75390625, 84.72265625);
    assertEquals(expectedPartition, actualPartition);
    
    BoundingBox<BlockLocation> expectedBounds = BoundingBox.of(
        BlockLocation.of(-16384.0, -16384.0),
        BlockLocation.of(+16384.0, +16384.0));
    BoundingBox<BlockLocation> actualBounds = actualPartition.getBoundingBox();
    assertEquals(expectedBounds, actualBounds);
  }

}
