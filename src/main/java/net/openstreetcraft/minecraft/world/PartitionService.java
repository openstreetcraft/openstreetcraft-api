// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.world;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.projection.ProjectedLocation;
import net.openstreetcraft.terrarium.TerrariumImage;
import net.openstreetcraft.terrarium.TerrariumValue;
import net.openstreetcraft.tms.api.TileService;
import net.openstreetcraft.tms.model.Tile;
import net.openstreetcraft.tms.model.TileMap;

@Service
@RequiredArgsConstructor
public class PartitionService {

  private static final double THRESHOLD = 256 - 32;
  
  private final TileMap tileMap;
  private final TileService tileService;

  public Optional<Partition> findPartitionAtLocation(ProjectedLocation location)
      throws IOException {
    List<Tile> tiles = tileMap.getTiles(location.getX(), location.getY());
    return search(tiles, 6, 18);
  }

  private Optional<Partition> search(List<Tile> tiles, int low, int high) throws IOException {
    if (high < low) {
      return Optional.empty();
    }

    int middle = low  + ((high - low) / 2);
    Tile tile = tiles.get(middle);

    double minHeight = Double.POSITIVE_INFINITY;
    double maxHeight = Double.NEGATIVE_INFINITY;

    for (Double value : getValues(tile)) {
      minHeight = Math.min(minHeight, value);
      maxHeight = Math.max(maxHeight, value);
    }
    
    boolean found = maxHeight < 0 && minHeight < 0;
    found |= maxHeight - minHeight < THRESHOLD;
    
    if (!found) {
      return search(tiles, middle + 1, high);
    }
    
    Optional<Partition> other = search(tiles, low, middle - 1);
    if (other.isPresent()) {
      return other;
    }
    
    return Optional.of(new Partition(tile, boundingBox(tile), minHeight, maxHeight));
  }

  private List<Double> getValues(Tile tile) throws IOException {
    return getValues(tileService.getBufferedImage(tileMap, tile));
  }

  private static List<Double> getValues(BufferedImage image) {
    return getValues(new TerrariumImage(image), image.getWidth(), image.getHeight());
  }

  private static List<Double> getValues(TerrariumImage image, int width, int height) {
    return getValues(image.getValues(0, 0, width, height));
  }

  private static List<Double> getValues(TerrariumValue[] values) {
    return Arrays.stream(values)
        .map(TerrariumValue::toDouble)
        .filter(v -> !v.isNaN())
        .collect(Collectors.toList());
  }

  private BoundingBox<ProjectedLocation> boundingBox(Tile tile) {
    net.openstreetcraft.tms.model.BoundingBox bounds = tileMap.boundingBox(tile);
    return BoundingBox.of(
        ProjectedLocation.of(bounds.getMinx(), bounds.getMaxy()),
        ProjectedLocation.of(bounds.getMaxx(), bounds.getMiny()));
  }

}
