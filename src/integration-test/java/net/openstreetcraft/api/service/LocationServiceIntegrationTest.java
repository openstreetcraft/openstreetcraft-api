// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.projection.SphericalLocation;

public class LocationServiceIntegrationTest {

  private RestTemplateFactory restTemplateFactory = new RestTemplateFactory();
  private LocationService locationService;

  @Before
  public void setUp() {
    locationService = new LocationService(
        restTemplateFactory.getRestTemplate(),
        restTemplateFactory.getApiGateway());
  }

  @Test
  public void getsLocationByAddress() {
    String address = "36001 Amphitheatre Parkway, Mountain View, CA 94043, USA";
    SphericalLocation expected = SphericalLocation.of(-122.085523, 37.422674);
    SphericalLocation actual = locationService.getLocation(address);
    assertEquals(expected, actual);
  }

  @Test
  public void getsAddressByLocation() {
    String expected = "36001 Amphitheatre Pkwy, Mountain View, CA 94043, USA";
    SphericalLocation location = SphericalLocation.of(-122.085523, 37.422674);
    String actual = locationService.getAddress(location);
    assertEquals(expected, actual);
  }

}
