// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.util;

import static org.junit.Assert.assertEquals;

import java.io.InputStream;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import de.ixilon.osm.schema.ObjectFactory;
import de.ixilon.osm.schema.Osm;
import de.ixilon.osm.schema.OsmAdapter;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmRoot;
import de.ixilon.osm.schema.OsmWay;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Unmarshaller;

public class OsmUtilsTest {

  private Osm osm;

  @Before
  public void setUp() throws Exception {
    JAXBContext context = JAXBContextFactory.createContext(new Class[] {ObjectFactory.class}, null);
    Unmarshaller unmarshaller = context.createUnmarshaller();
    try (InputStream resourceAsStream = getClass().getResourceAsStream("osm.xml")) {
      XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
      XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(resourceAsStream);
      @SuppressWarnings("unchecked")
      JAXBElement<OsmRoot> response =
          (JAXBElement<OsmRoot>) unmarshaller.unmarshal(xmlStreamReader);
      osm = new OsmAdapter(response.getValue());
    }
  }

  @Test
  public void extractsNodes() {
    List<OsmNode> expected = ImmutableList.of(
        node(4217868255L, 48.2649760, 11.4346980),
        node(4217868256L, 48.2651044, 11.4349653),
        node(4217868257L, 48.2650554, 11.4350184),
        // node(4217868258L, 48.2649928, 11.4348881),
        node(4217868259L, 48.2649654, 11.4349179),
        node(4217868260L, 48.2648996, 11.4347809));
    List<OsmNode> actual = ImmutableList.copyOf(OsmUtils.getNodes(osm));

    assertEquals(expected.size(), actual.size());
    
    OsmNode node = actual.get(0);
    
    assertEquals(4217868255L, node.getId());
    assertEquals(48.2649760, node.getLat(), Double.MIN_VALUE);
    assertEquals(11.4346980, node.getLon(), Double.MIN_VALUE);
  }

  @Test
  public void extractsWays() {
    List<OsmWay> expected = ImmutableList.of(way(422062125L));
    List<OsmWay> actual = ImmutableList.copyOf(OsmUtils.getWays(osm));

    assertEquals(expected.size(), actual.size());
    
    OsmWay way = actual.get(0);
    
    assertEquals(422062125L, way.getId());
  }

  private static OsmNode node(long id, double lat, double lon) {
    OsmNode node = new OsmNode();
    node.setId(id);
    node.setLat(lat);
    node.setLon(lon);
    return node;
  }

  private static OsmWay way(long id) {
    OsmWay way = new OsmWay();
    way.setId(id);
    return way;
  }

}
