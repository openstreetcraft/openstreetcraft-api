// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.rest;

import org.apache.http.HttpHost;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;

public class RestTemplateFactory {

  private static final String PACKAGE = "net.openstreetcraft.api";
  private final AnnotationConfigApplicationContext ctx;

  public RestTemplateFactory() {
    ctx = new AnnotationConfigApplicationContext(PACKAGE);
  }

  /**
   * Scan class files with a custom class loader. 
   */
  public RestTemplateFactory(ClassLoader classLoader) {
    ctx = new AnnotationConfigApplicationContext();
    ctx.setClassLoader(classLoader);
    ctx.scan(PACKAGE);
    ctx.refresh();
  }

  /**
   * Create REST template connecting to the Openstreetcraft API gateway.
   */
  public RestTemplate getRestTemplate() {
    return ctx.getBean(RestTemplate.class);
  }

  /**
   * Create HttpHost endpoint of the Openstreetcraft API gateway.
   */
  public HttpHost getApiGateway() {
    return ctx.getBean(HttpHost.class);
  }

}
