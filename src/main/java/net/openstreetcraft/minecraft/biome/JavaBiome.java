// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.biome;

/**
 * Biomes of Java edition. 
 */
public enum JavaBiome implements IdentifiableBiome {
  BADLANDS(37),
  BADLANDS_PLATEAU(39),
  BAMBOO_JUNGLE(168),
  BAMBOO_JUNGLE_HILLS(169),
  BASALT_DELTAS(173),
  BEACH(16),
  BIRCH_FOREST(27),
  BIRCH_FOREST_HILLS(28),
  COLD_OCEAN(46),
  CRIMSON_FOREST(171),
  DARK_FOREST(29),
  DARK_FOREST_HILLS(157),
  DEEP_COLD_OCEAN(49),
  DEEP_FROZEN_OCEAN(50),
  DEEP_LUKEWARM_OCEAN(48),
  DEEP_OCEAN(24),
  DEEP_WARM_OCEAN(47),
  DESERT(2),
  DESERT_HILLS(17),
  DESERT_LAKES(130),
  END_BARRENS(43),
  END_HIGHLANDS(42),
  END_MIDLANDS(41),
  ERODED_BADLANDS(165),
  FLOWER_FOREST(132),
  FOREST(4),
  FROZEN_OCEAN(10),
  FROZEN_RIVER(11),
  GIANT_SPRUCE_TAIGA(160),
  GIANT_SPRUCE_TAIGA_HILLS(161),
  GIANT_TREE_TAIGA(32),
  GIANT_TREE_TAIGA_HILLS(33),
  GRAVELLY_MOUNTAINS(131),
  ICE_SPIKES(140),
  JUNGLE(21),
  JUNGLE_EDGE(23),
  JUNGLE_HILLS(22),
  LUKEWARM_OCEAN(45),
  MODIFIED_BADLANDS_PLATEAU(167),
  MODIFIED_GRAVELLY_MOUNTAINS(162),
  MODIFIED_JUNGLE(149),
  MODIFIED_JUNGLE_EDGE(151),
  MODIFIED_WOODED_BADLANDS_PLATEAU(166),
  MOUNTAIN_EDGE(20),
  MOUNTAINS(3),
  MUSHROOM_FIELDS(14),
  MUSHROOM_FIELD_SHORE(15),
  NETHER_WASTES(8),
  OCEAN(0),
  PLAINS(1),
  RIVER(7),
  SAVANNA(35),
  SAVANNA_PLATEAU(36),
  SHATTERED_SAVANNA(163),
  SHATTERED_SAVANNA_PLATEAU(164),
  SMALL_END_ISLANDS(40),
  SNOWY_BEACH(26),
  SNOWY_MOUNTAINS(13),
  SNOWY_TAIGA(30),
  SNOWY_TAIGA_HILLS(31),
  SNOWY_TAIGA_MOUNTAINS(158),
  SNOWY_TUNDRA(12),
  SOUL_SAND_VALLEY(170),
  STONE_SHORE(25),
  SUNFLOWER_PLAINS(129),
  SWAMP(6),
  SWAMP_HILLS(134),
  TAIGA(5),
  TAIGA_HILLS(19),
  TAIGA_MOUNTAINS(133),
  TALL_BIRCH_FOREST(155),
  TALL_BIRCH_HILLS(156),
  THE_END(9),
  THE_VOID(127),
  WARM_OCEAN(44),
  WARPED_FOREST(172),
  WOODED_BADLANDS_PLATEAU(38),
  WOODED_HILLS(18),
  WOODED_MOUNTAINS(34);

  private final int ident;

  private JavaBiome(int ident) {
    this.ident = ident;
  }

  @Override
  public int ident() {
    return ident;
  }
}
