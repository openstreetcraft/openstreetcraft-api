// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.ixilon.osm.schema.OsmNode;

public class OsmNodeListTest {

  @Test
  public void findsNode() {
    OsmNodeList nodes = OsmNodeList.of(node(1), node(2), node(3));
    long expected = 2;
    long actual = nodes.findNode(2).get().getId();
    assertEquals(expected, actual);
  }

  private static OsmNode node(long id) {
    OsmNode node = new OsmNode();
    node.setId(id);
    return node;
  }
}
