// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

public class Circle2D {

  private final double xcenter;
  private final double ycenter;
  private final double radius;

  /**
   * Create circle around the center of a given rectangle.
   */
  public Circle2D(Rectangle center, double radius) {
    this.xcenter = center.toRectangle2D().getCenterX();
    this.ycenter = center.toRectangle2D().getCenterY();
    this.radius = radius;
  }

  /**
   * Create circle around a given center point.
   */
  public Circle2D(double xcenter, double ycenter, double radius) {
    this.xcenter = xcenter;
    this.ycenter = ycenter;
    this.radius = radius;
  }

  /**
   * Check if a line segment between two points intersects this circle.
   */
  public boolean intersectsLine(double x1, double y1, double x2, double y2) {
    return intersectsLine(new Vector2D(x1, y1), new Vector2D(x2, y2));
  }

  /**
   * Check if a line segment between two points intersects this circle.
   * @see <a href=
   *      "http://stackoverflow.com/questions/1073336/circle-line-segment-collision-detection-algorithm">
   *      Circle line-segment collision detection algorithm</a>
   */
  public boolean intersectsLine(Vector2D p1, Vector2D p2) {
    Vector2D d = p2.diff(p1);
    Vector2D f = p1.diff(new Vector2D(xcenter, ycenter));

    double a = d.dot(d);
    double b = 2 * f.dot(d);
    double c = f.dot(f) - radius * radius;
    double dis = b * b - 4 * a * c;

    if (dis < 0) {
      return false;
    }

    dis = Math.sqrt(dis);

    double t1 = (-b - dis) / (2 * a);
    double t2 = (-b + dis) / (2 * a);

    if (t1 >= 0 && t1 <= 1) {
      return true;
    }

    if (t2 >= 0 && t2 <= 1) {
      return true;
    }

    return t1 < 0 && t2 > 1;
  }

}
