// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.util;

import com.google.common.base.Predicate;

import de.ixilon.osm.schema.Osm;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmRelation;
import de.ixilon.osm.schema.OsmRoot;
import de.ixilon.osm.schema.OsmWay;

public final class OsmUtils {

  private static final Predicate<Object> IS_NODE = new Predicate<Object>() {
    @Override
    public boolean apply(Object input) {
      return input != null && OsmNode.class.isAssignableFrom(input.getClass())
          && isVisible((OsmNode) input);
    }

    private boolean isVisible(OsmNode node) {
      Boolean visible = node.isVisible();
      return (visible == null) || visible;
    }
  };

  private static final Predicate<Object> IS_WAY = new Predicate<Object>() {
    @Override
    public boolean apply(Object input) {
      return input != null && OsmWay.class.isAssignableFrom(input.getClass())
          && isVisible((OsmWay) input);
    }

    private boolean isVisible(OsmWay way) {
      Boolean visible = way.isVisible();
      return (visible == null) || visible;
    }
  };

  private static final Predicate<Object> IS_RELATION = new Predicate<Object>() {
    @Override
    public boolean apply(Object input) {
      return input != null && OsmRelation.class.isAssignableFrom(input.getClass())
          && isVisible((OsmRelation) input);
    }

    private boolean isVisible(OsmRelation relation) {
      Boolean visible = relation.isVisible();
      return (visible == null) || visible;
    }
  };

  private OsmUtils() {
    // static utility class
  }

  /**
   * Extract nodes. 
   */
  @Deprecated
  public static Iterable<OsmNode> getNodes(Osm osm) {
    return Filter.filter(osm.getNodesAndRelationsAndWaies(), IS_NODE);
  }

  /**
   * Extract nodes. 
   */
  public static Iterable<OsmNode> getNodes(OsmRoot osm) {
    return Filter.filter(osm.getNodeOrRelationOrWay(), IS_NODE);
  }

  /**
   * Extract ways. 
   */
  @Deprecated
  public static Iterable<OsmWay> getWays(Osm osm) {
    return Filter.filter(osm.getNodesAndRelationsAndWaies(), IS_WAY);
  }

  /**
   * Extract ways. 
   */
  public static Iterable<OsmWay> getWays(OsmRoot osm) {
    return Filter.filter(osm.getNodeOrRelationOrWay(), IS_WAY);
  }

  /**
   * Extract relations. 
   */
  @Deprecated
  public static Iterable<OsmRelation> getRelations(Osm osm) {
    return Filter.filter(osm.getNodesAndRelationsAndWaies(), IS_RELATION);
  }

  /**
   * Extract relations. 
   */
  public static Iterable<OsmRelation> getRelations(OsmRoot osm) {
    return Filter.filter(osm.getNodeOrRelationOrWay(), IS_RELATION);
  }

}
