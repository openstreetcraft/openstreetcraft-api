// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

public class RectangularBoundingBox extends BoundingBox {

  private RectangularBoundingBox(Builder builder) {
    super(createParentBuilder(builder));
  }

  private static BoundingBox.Builder createParentBuilder(Builder builder) {
    Point upperLeft = new Point(builder.lowerLeft.getX(), builder.upperRight.getY());
    Point lowerRight = new Point(builder.upperRight.getX(), builder.lowerLeft.getY());

    return new BoundingBox.Builder()
        .withLowerLeft(builder.lowerLeft)
        .withUpperRight(builder.upperRight)
        .withLowerRight(lowerRight)
        .withUpperLeft(upperLeft);
  }

  /**
   * Get the center point of the rectangle of the bounding box. 
   */
  public Point center() {
    return new Point(
        getX() + width() / 2.0,
        getY() + height() / 2.0);
  }

  /**
   * Check if the point is inside the rectangleof the bounding box.
   */
  public boolean contains(Point point) {
    return point.getX() >= getX()
        && point.getY() >= getY()
        && point.getX() <= getX() + width()
        && point.getY() <= getY() + height();
  }

  public double getX() {
    return getLowerLeft().getX();
  }
  
  public double getY() {
    return getLowerLeft().getY();
  }
  
  public double width() {
    return getLowerRight().getX() - getLowerLeft().getX();
  }
  
  public double height() {
    return getUpperLeft().getY() - getLowerLeft().getY();
  }
  
  public static class Builder {

    private Point upperRight;
    private Point lowerLeft;

    public RectangularBoundingBox build() {
      return new RectangularBoundingBox(this);
    }

    public Builder withUpperRight(Point point) {
      this.upperRight = point;
      return this;
    }

    public Builder withLowerLeft(Point point) {
      this.lowerLeft = point;
      return this;
    }
  }

}
