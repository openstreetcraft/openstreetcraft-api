// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.biome;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BiomeConverterTest {

  private final BiomeConverter converter = new BiomeConverter();

  @Test
  public void convertsByteToBiome() {
    assertEquals(Biome.OCEAN, converter.apply((byte) 0));
    assertEquals(Biome.PLAINS, converter.apply((byte) 1));
    assertEquals(Biome.FOREST, converter.apply((byte) 4));
    assertEquals(Biome.SWAMP, converter.apply((byte) 6));
    assertEquals(Biome.RIVER, converter.apply((byte) 7));
    assertEquals(Biome.BEACH, converter.apply((byte) 16));
  }
}
