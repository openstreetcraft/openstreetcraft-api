// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.biome;

/**
 * Biomes of Bedrock edition. 
 */
public enum BedrockBiome implements IdentifiableBiome {
  BAMBOO_JUNGLE(168),
  BAMBOO_JUNGLE_HILLS(169),
  BASALT_DELTAS(181),
  BEACH(16),
  BIRCH_FOREST(27),
  BIRCH_FOREST_HILLS(28),
  BIRCH_FOREST_HILLS_MUTATED(156),
  BIRCH_FOREST_MUTATED(155),
  COLD_BEACH(26),
  COLD_OCEAN(46),
  COLD_TAIGA(30),
  COLD_TAIGA_HILLS(31),
  COLD_TAIGA_MUTATED(158),
  CRIMSON_FOREST(179),
  DEEP_COLD_OCEAN(49),
  DEEP_FROZEN_OCEAN(50),
  DEEP_LUKEWARM_OCEAN(48),
  DEEP_OCEAN(24),
  DEEP_WARM_OCEAN(47),
  DESERT(2),
  DESERT_HILLS(17),
  DESERT_MUTATED(130),
  EXTREME_HILLS(3),
  EXTREME_HILLS_EDGE(20),
  EXTREME_HILLS_MUTATED(131),
  EXTREME_HILLS_PLUS_TREES(34),
  EXTREME_HILLS_PLUS_TREES_MUTATED(162),
  FLOWER_FOREST(132),
  FOREST(4),
  FOREST_HILLS(18),
  FROZEN_OCEAN(10),
  FROZEN_RIVER(11),
  HELL(8),
  ICE_MOUNTAINS(13),
  ICE_PLAINS(12),
  ICE_PLAINS_SPIKES(140),
  JUNGLE(21),
  JUNGLE_EDGE(23),
  JUNGLE_EDGE_MUTATED(151),
  JUNGLE_HILLS(22),
  JUNGLE_MUTATED(149),
  LEGACY_FROZEN_OCEAN(43),
  LUKEWARM_OCEAN(45),
  MEGA_TAIGA(32),
  MEGA_TAIGA_HILLS(33),
  MESA(37),
  MESA_BRYCE(165),
  MESA_PLATEAU(39),
  MESA_PLATEAU_MUTATED(167),
  MESA_PLATEAU_STONE(38),
  MESA_PLATEAU_STONE_MUTATED(166),
  MUSHROOM_ISLAND(14),
  MUSHROOM_ISLAND_SHORE(15),
  OCEAN(42),
  PLAINS(1),
  REDWOOD_TAIGA_HILLS_MUTATED(161),
  REDWOOD_TAIGA_MUTATED(160),
  RIVER(7),
  ROOFED_FOREST(29),
  ROOFED_FOREST_MUTATED(157),
  SAVANNA(35),
  SAVANNA_MUTATED(163),
  SAVANNA_PLATEAU(36),
  SAVANNA_PLATEAU_MUTATED(164),
  SOULSAND_VALLEY(178),
  STONE_BEACH(25),
  SUNFLOWER_PLAINS(129),
  SWAMP(6),
  SWAMP_MUTATED(134),
  TAIGA(5),
  TAIGA_HILLS(19),
  TAIGA_MUTATED(133),
  THE_END(9 ),
  WARM_OCEAN(44),
  WARPED_FOREST(180);
  
  private final int ident;

  private BedrockBiome(int ident) {
    this.ident = ident;
  }

  @Override
  public int ident() {
    return ident;
  }
}
