// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.world;

import java.io.IOException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.openstreetcraft.tms.api.TileMapRepository;
import net.openstreetcraft.tms.api.TileService;
import net.openstreetcraft.tms.api.TileServiceFactory;
import net.openstreetcraft.tms.model.TileMap;

@Configuration
public class TileServiceConfiguration {

  @Bean
  public TileService getTileService(TileServiceFactory factory, TileMap map) {
    return factory.createTileService(map);
  }

  @Bean
  public TileMap getTileMap(TileMapRepository repository) throws IOException {
    String map = "earth-elevation";
    return repository.find(map);
  }

  @Bean
  public TileMapRepository geTileMapRepository() {
    return new TileMapRepository();
  }

  @Bean
  public TileServiceFactory getTileServiceFactory() {
    return new TileServiceFactory();
  }

}
