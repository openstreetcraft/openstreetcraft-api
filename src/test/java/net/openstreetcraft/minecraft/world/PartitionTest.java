// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.world;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.openstreetcraft.coordinates.BlockLocation;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.projection.ProjectedLocation;
import net.openstreetcraft.tms.model.Tile;

public class PartitionTest {

  ObjectMapper mapper = new ObjectMapper();
  Partition partition;
  
  @Before
  public void setUp() {
    partition = new Partition(
        new Tile(1, 2, 3),
        BoundingBox.of(
            ProjectedLocation.of(200.0, 300.0),
            ProjectedLocation.of(500.0, 100.0)),
        10.0,
        20.0);
  }

  @Test
  public void deserialize() throws IOException {
    String json = "{"
        + "\"world\":\"EARTH\","
        + "\"name\":\"EARTH-3-1-2\","
        + "\"origin\":{\"x\":350.0,\"y\":200.0},"
        + "\"boundingBox\":{\"nw\":{\"x\":-150.0,\"z\":-100.0},\"so\":{\"x\":150.0,\"z\":100.0}},"
        + "\"minHeight\":10.0,"
        + "\"maxHeight\":20.0}";
    Partition actual = mapper.readValue(json, Partition.class);
    assertEquals(partition, actual);
  }

  @Test
  public void serialize() throws IOException {
    String expected = "{"
        + "\"world\":\"EARTH\","
        + "\"name\":\"EARTH-3-1-2\","
        + "\"origin\":{\"x\":350.0,\"y\":200.0},"
        + "\"boundingBox\":{\"nw\":{\"x\":-150.0,\"z\":-100.0},\"so\":{\"x\":150.0,\"z\":100.0}},"
        + "\"minHeight\":10.0,"
        + "\"maxHeight\":20.0}";
    String actual = mapper.writeValueAsString(partition);
    assertEquals(expected, actual);
  }

  @Test
  public void getMinHeight() {
    assertEquals(10.0 , partition.getMinHeight(), Double.MIN_VALUE);
  }

  @Test
  public void getMaxHeight() {
    assertEquals(20.0 , partition.getMaxHeight(), Double.MIN_VALUE);
  }

  @Test
  public void getBoundingBox() {
    BoundingBox<BlockLocation> expected = BoundingBox.of(
        BlockLocation.of(-150.0, -100.0),
        BlockLocation.of(150.0, 100.0));
    assertEquals(expected , partition.getBoundingBox());
  }

  @Test
  public void globalLocation() {
    ProjectedLocation actual = partition.globalLocation(BlockLocation.of(50.0, 80.0));
    ProjectedLocation expected = ProjectedLocation.of(400.0, 120.0);
    assertEquals(expected, actual);
  }

  @Test
  public void localLocation() {
    BlockLocation actual = partition.localLocation(ProjectedLocation.of(400.0, 120.0));
    BlockLocation expected = BlockLocation.of(50.0, 80.0);
    assertEquals(expected, actual);
  }

}
