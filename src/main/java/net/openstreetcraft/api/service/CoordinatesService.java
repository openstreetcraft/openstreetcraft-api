// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.coordinates.BlockLocation;
import net.openstreetcraft.minecraft.world.Partition;
import net.openstreetcraft.projection.ProjectedLocation;
import net.openstreetcraft.projection.Projection;
import net.openstreetcraft.projection.ProjectionBuilder;
import net.openstreetcraft.projection.SphericalLocation;

/**
 * Service facade of the Openstreetcraft coordinates webservice.
 */
@Service
public class CoordinatesService {

  private final Map<String, Projection> projections = new HashMap<>();

  @SuppressWarnings("unused")
  private final RestTemplate restTemplate;

  @Autowired
  public CoordinatesService(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  private Projection projection(Partition partition) {
    String map = partition.getWorld().name();
    // TODO: Projection using partition.getOrigin()
    return projections.computeIfAbsent(map, key -> new ProjectionBuilder(map).build());
  }

  /**
   * Transforms list of Minecraft coordinates into list of spherical coordinates.
   */
  public List<SphericalLocation> getSphericalLocations(List<BlockLocation> locations,
      Partition partition) {
    return locations.stream()
        .map(v -> getSphericalLocation(v, partition))
        .collect(Collectors.toList());
  }

  /**
   * Transforms Minecraft coordinate into spherical coordinate.
   */
  public SphericalLocation getSphericalLocation(BlockLocation location, Partition partition) {
    return projection(partition).transform(partition.globalLocation(location));
  }

  /**
   * Transforms list of spherical coordinates into list of Minecraft coordinates.
   */
  public List<BlockLocation> getMinecraftLocations(List<SphericalLocation> locations,
      Partition partition) {
    return locations.stream()
        .map(v -> getMinecraftLocation(v, partition))
        .collect(Collectors.toList());
  }

  /**
   * Transforms spherical coordinate into Minecraft coordinate.
   */
  public BlockLocation getMinecraftLocation(SphericalLocation location, Partition partition) {
    return partition.localLocation(projection(partition).transform(location));
  }

}
