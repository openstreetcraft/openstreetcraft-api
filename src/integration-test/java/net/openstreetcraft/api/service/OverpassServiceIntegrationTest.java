// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import de.ixilon.osm.schema.Osm;
import net.openstreetcraft.api.geom.RectangularBoundingBox;
import net.openstreetcraft.api.geom.SphericalPoint;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.projection.SphericalLocation;

public class OverpassServiceIntegrationTest {

  private RestTemplateFactory restTemplateFactory = new RestTemplateFactory();
  private OverpassService overpassService;

  @Before
  public void setUp() {
    overpassService = new OverpassService(
        restTemplateFactory.getRestTemplate(),
        restTemplateFactory.getApiGateway());
  }

  @Test
  public void getOpenStreetMapData() {
    BoundingBox<SphericalLocation> bbox = BoundingBox.of(
        SphericalLocation.of(11.4348, 48.2649),
        SphericalLocation.of(11.4347, 48.2650));

    Osm actual = overpassService.getOsm(bbox);

    assertFalse(actual.getNodesAndRelationsAndWaies().isEmpty());

    // TODO: argument catcher
    // Osm expected = readTestData("overpass.xml");
    // assertEquals(expected, actual);
  }

//  private Osm readTestData(String filename) throws Exception {
//    JAXBContext context = JAXBContext.newInstance(ObjectFactory.class);
//    Unmarshaller unmarshaller = context.createUnmarshaller();
//    try (InputStream resourceAsStream = getClass().getResourceAsStream(filename)) {
//      XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
//      XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(resourceAsStream);
//      return (Osm) unmarshaller.unmarshal(xmlStreamReader);
//    }
//  }

}
