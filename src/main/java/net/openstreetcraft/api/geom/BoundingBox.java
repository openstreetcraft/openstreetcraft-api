// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import static java.util.Objects.requireNonNull;

import java.util.Objects;

public class BoundingBox {

  private final Point upperLeft;
  private final Point upperRight;
  private final Point lowerLeft;
  private final Point lowerRight;

  protected BoundingBox(Builder builder) {
    this.upperLeft = requireNonNull(builder.upperLeft);
    this.upperRight = requireNonNull(builder.upperRight);
    this.lowerLeft = requireNonNull(builder.lowerLeft);
    this.lowerRight = requireNonNull(builder.lowerRight);
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(upperLeft, upperRight, lowerLeft, lowerRight);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof BoundingBox)) {
      return false;
    }

    BoundingBox other = (BoundingBox) obj;

    return Objects.equals(this.upperLeft, other.upperLeft)
        && Objects.equals(this.upperRight, other.upperRight)
        && Objects.equals(this.lowerLeft, other.lowerLeft)
        && Objects.equals(this.lowerRight, other.lowerRight);
  }

  @Override
  public String toString() {
    return "BoundingBox [upperLeft=" + upperLeft + ", upperRight=" + upperRight + ", lowerLeft="
        + lowerLeft + ", lowerRight=" + lowerRight + "]";
  }

  public Point getUpperLeft() {
    return upperLeft;
  }

  public Point getUpperRight() {
    return upperRight;
  }

  public Point getLowerLeft() {
    return lowerLeft;
  }

  public Point getLowerRight() {
    return lowerRight;
  }

  public static class Builder {
    private Point upperLeft;
    private Point upperRight;
    private Point lowerLeft;
    private Point lowerRight;

    public BoundingBox build() {
      return new BoundingBox(this);
    }

    public Builder withUpperLeft(Point point) {
      this.upperLeft = point;
      return this;
    }

    public Builder withUpperRight(Point point) {
      this.upperRight = point;
      return this;
    }

    public Builder withLowerLeft(Point point) {
      this.lowerLeft = point;
      return this;
    }

    public Builder withLowerRight(Point point) {
      this.lowerRight = point;
      return this;
    }
  }

}
