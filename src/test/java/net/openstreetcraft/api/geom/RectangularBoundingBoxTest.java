// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class RectangularBoundingBoxTest {

  RectangularBoundingBox bbox;

  @Before
  public void setUp() {
    bbox = new RectangularBoundingBox.Builder()
        .withLowerLeft(new Point(1, 2))
        .withUpperRight(new Point(3, 5))
        .build();
  }

  @Test
  public void getsLowerLeftPoint() {
    Point actual = bbox.getLowerLeft();
    Point expected = new Point(1, 2);
    assertEquals(expected, actual);
  }

  @Test
  public void getsUpperLeftPoint() {
    Point actual = bbox.getUpperLeft();
    Point expected = new Point(1, 5);
    assertEquals(expected, actual);
  }

  @Test
  public void getsLowerRightPoint() {
    Point actual = bbox.getLowerRight();
    Point expected = new Point(3, 2);
    assertEquals(expected, actual);
  }

  @Test
  public void getsUpperRightPoint() {
    Point actual = bbox.getUpperRight();
    Point expected = new Point(3, 5);
    assertEquals(expected, actual);
  }

  @Test
  public void getsCenterPoint() {
    Point actual = bbox.center();
    Point expected = new Point(2.0, 3.5);
    assertEquals(expected, actual);
  }

  @Test
  public void getsWidth() {
    double actual = bbox.width();
    double expected = 2;
    assertEquals(expected, actual, Double.MIN_VALUE);
  }

  @Test
  public void getsHeight() {
    double actual = bbox.height();
    double expected = 3;
    assertEquals(expected, actual, Double.MIN_VALUE);
  }

  @Test
  public void getsX() {
    double actual = bbox.getX();
    double expected = 1;
    assertEquals(expected, actual, Double.MIN_VALUE);
  }

  @Test
  public void getsY() {
    double actual = bbox.getY();
    double expected = 2;
    assertEquals(expected, actual, Double.MIN_VALUE);
  }

  @Test
  public void containsLowerLeftPoint() {
    assertTrue(bbox.contains(bbox.getLowerLeft()));
  }

  @Test
  public void containsUpperLeftPoint() {
    assertTrue(bbox.contains(bbox.getUpperLeft()));
  }

  @Test
  public void containsLowerRightPoint() {
    assertTrue(bbox.contains(bbox.getLowerRight()));
  }

  @Test
  public void containsUpperRightPoint() {
    assertTrue(bbox.contains(bbox.getUpperRight()));
  }

  @Test
  public void containsCenterPoint() {
    assertTrue(bbox.contains(bbox.center()));
  }

  @Test
  public void excludeOuterPoint() {
    assertFalse(bbox.contains(new Point(1.0, 1.0)));
  }

}
