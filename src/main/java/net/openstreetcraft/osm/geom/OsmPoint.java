// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import de.ixilon.osm.schema.OsmNode;

public class OsmPoint {

  private final OsmNode node;
  private final Point2D point;

  /**
   * The spherical location of a node. 
   */
  public OsmPoint(OsmNode node) {
    this.node = node;
    this.point = new Point2D.Double(node.getLon(), node.getLat());
  }

  /**
   * Returns true if point is inside of the bounding box.
   */
  public boolean isInside(Rectangle bbox) {
    Rectangle2D rect = bbox.toRectangle2D();
    return point.getX() >= rect.getMinX() && point.getX() < rect.getMaxX()
        && point.getY() >= rect.getMinY() && point.getY() < rect.getMaxY();
  }

  public OsmNode toNode() {
    return node;
  }

  public Point2D toPoint2D() {
    return point;
  }

}
