// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.coordinates.BlockLocation;
import net.openstreetcraft.minecraft.world.Partition;
import net.openstreetcraft.minecraft.world.PartitionService;
import net.openstreetcraft.minecraft.world.TestPartitionService;
import net.openstreetcraft.projection.SphericalLocation;

public class CoordinatesServiceIntegrationTest {

  private RestTemplateFactory restTemplateFactory = new RestTemplateFactory();
  private PartitionService partitionService;
  private Partition partition;
  private CoordinatesService coordinatesService;
  
  @Before
  public void setUp() throws IOException {
    partitionService = new TestPartitionService();
    partition = partitionService.findPartitionAtLocation(null).get();
    coordinatesService = new CoordinatesService(restTemplateFactory.getRestTemplate());
  }

  @Test
  public void getsSphericalLocationOfMinecraftLocation() {
    BlockLocation location = BlockLocation.of(0.0, 0.0);
    SphericalLocation expected = SphericalLocation.of(0.0, 0.0); 
    SphericalLocation actual = coordinatesService.getSphericalLocation(location, partition);
    assertEquals(expected, actual);
  }

  @Test
  public void getsMinecraftLocationOfSphericalLocation() {
    SphericalLocation location = SphericalLocation.of(0, 0);
    BlockLocation expected = BlockLocation.of(0.0, -0.0); 
    BlockLocation actual = coordinatesService.getMinecraftLocation(location, partition);
    assertEquals(expected, actual);
  }


}
