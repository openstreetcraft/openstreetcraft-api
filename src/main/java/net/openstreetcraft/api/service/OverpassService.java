// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.temporal.ChronoUnit;

import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.esri.core.geometry.Envelope2D;
import com.google.common.annotations.VisibleForTesting;

import de.ixilon.osm.api.OverpassClient;
import de.ixilon.osm.schema.Osm;
import de.ixilon.osm.schema.OsmAdapter;
import de.ixilon.osm.schema.OsmRoot;
import net.jodah.failsafe.Failsafe;
import net.jodah.failsafe.RetryPolicy;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.projection.SphericalLocation;

@Service
public class OverpassService {

  private static final int DIGITS = 3;
  private static final BigDecimal FACTOR = BigDecimal.ONE.movePointLeft(DIGITS);
  private static final RetryPolicy<Object> RETRY_POLICY = new RetryPolicy<>()
      .handle(RestClientException.class)
      .withBackoff(1, 30, ChronoUnit.SECONDS)
      .withMaxRetries(3);
  
  private final OverpassClient xapiClient;
  private final OverpassClient xmlClient;

  @Autowired
  public OverpassService(RestTemplate restTemplate, HttpHost gateway) {
    xapiClient = new OverpassClient(endpoint(gateway, "xapi"), restTemplate);
    xmlClient = new OverpassClient(endpoint(gateway, "interpreter"), restTemplate);
  }
  
  private static URI endpoint(HttpHost gateway, String path) {
    try {
      return new URI(String.format("%s/overpass/%s", gateway.toURI(), path));
    } catch (URISyntaxException exception) {
      throw new IllegalStateException(exception);
    }
  }

  /**
   * Returns OpenStreetMap data within a given bounding box.
   * @deprecated use getOsmRoot(bbox)
   */
  @Deprecated
  public Osm getOsm(BoundingBox<SphericalLocation> bbox) {
    return new OsmAdapter(getOsmRoot(bbox));
  }

  /**
   * Returns OpenStreetMap data within a given bounding box.
   * @deprecated use getOsmRoot(bbox)
   */
  @Deprecated
  public Osm getOsm(Envelope2D bbox) {
    return new OsmAdapter(getOsmRoot(bbox));
  }

  /**
   * Returns OpenStreetMap data within a given bounding box.
   */
  public OsmRoot getOsmRoot(BoundingBox<SphericalLocation> bbox) {
    double xmin = bbox.getNw().getLongitude();
    double xmax = bbox.getSo().getLongitude();
    double ymin = bbox.getSo().getLatitude();
    double ymax = bbox.getNw().getLatitude();
    return getOsmRoot(new Envelope2D(xmin, ymin, xmax, ymax));
  }

  /**
   * Returns OpenStreetMap data within a given bounding box.
   */
  public OsmRoot getOsmRoot(Envelope2D bbox) {
    return getOsmRoot(query(round(bbox)));
  }

  protected OsmRoot getOsmRoot(String query) {
    return Failsafe.with(RETRY_POLICY).get(() -> xapiClient.get(query));
  }

  @VisibleForTesting
  static String query(Envelope2D bbox) {
    return String.format("*[bbox=%s,%s,%s,%s]", bbox.xmin, bbox.ymin, bbox.xmax, bbox.ymax);
  }

  @VisibleForTesting
  static Envelope2D round(Envelope2D bbox) {
    return new Envelope2D(
        roundDown(bbox.xmin),
        roundDown(bbox.ymin),
        roundUp(bbox.xmax),
        roundUp(bbox.ymax));
  }

  private static double roundDown(double value) {
    return BigDecimal.valueOf(value)
        .setScale(DIGITS, RoundingMode.FLOOR)
        .subtract(FACTOR)
        .doubleValue();
  }

  private static double roundUp(double value) {
    return BigDecimal.valueOf(value)
        .setScale(DIGITS, RoundingMode.CEILING)
        .add(FACTOR)
        .doubleValue();
  }

  /**
   * Returns OpenStreetMap data of a node by id.
   * @deprecated use getOsmRootByNode(nodeId)
   */
  @Deprecated
  public Osm getOsmByNode(long nodeId) {
    return new OsmAdapter(getOsmRootByNode(nodeId));
  }
  
  /**
   * Returns OpenStreetMap data of a way by id.
   * @deprecated use getOsmRootByWay(wayId)
   */
  @Deprecated
  public Osm getOsmByWay(long wayId) {
    return new OsmAdapter(getOsmRootByWay(wayId));
  }
  
  /**
   * Returns OpenStreetMap data of a node by id.
   */
  public OsmRoot getOsmRootByNode(long nodeId) {
    String format = "<id-query type='node' ref='%d'/>"
        + "<print/>";
    return postOsmRoot(String.format(format, nodeId));
  }

  /**
   * Returns OpenStreetMap data of a way by id.
   */
  public OsmRoot getOsmRootByWay(long wayId) {
    String format = "<union>"
        + "<id-query type='way' ref='%d'/>"
        + "<recurse type='way-node'/>"
        + "</union>"
        + "<print/>";
    return postOsmRoot(String.format(format, wayId));
  }

  protected OsmRoot postOsmRoot(String query) {
    return Failsafe.with(RETRY_POLICY).get(() -> xmlClient.post(query));
  }
}
