// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class Circle2DTest {

  // -o->
  @Test
  public void isInsideWhenLineImpalesCircle() {
    Vector2D p1 = new Vector2D(1, 1);
    Vector2D p2 = new Vector2D(5, 1);
    Circle2D circle = new Circle2D(3, 1, 1);

    assertTrue(circle.intersectsLine(p1, p2));
  }

  // --|--> |
  @Test
  public void isInsideWhenLinePokesCircle() {
    Vector2D p1 = new Vector2D(2, 1);
    Vector2D p2 = new Vector2D(4, 1);
    Circle2D circle = new Circle2D(4, 1, 1);

    assertTrue(circle.intersectsLine(p1, p2));
  }

  // | --|->
  @Test
  public void isInsideWhenLineExitsWoundOfCircle() {
    Vector2D p1 = new Vector2D(4, 1);
    Vector2D p2 = new Vector2D(6, 1);
    Circle2D circle = new Circle2D(4, 1, 1);

    assertTrue(circle.intersectsLine(p1, p2));
  }

  // -> o
  @Test
  public void isOutsideWhenLineFallsShortOfCircle() {
    Vector2D p1 = new Vector2D(1, 1);
    Vector2D p2 = new Vector2D(2, 1);
    Circle2D circle = new Circle2D(4, 1, 1);

    assertFalse(circle.intersectsLine(p1, p2));
  }

  // o ->
  @Test
  public void isOutsideWhenLinePastsCircle() {
    Vector2D p1 = new Vector2D(6, 1);
    Vector2D p2 = new Vector2D(7, 1);
    Circle2D circle = new Circle2D(4, 1, 1);

    assertFalse(circle.intersectsLine(p1, p2));
  }

  // | -> |
  @Test
  public void isInsideWhenLineIsCompletelyInsideOfCircle() {
    Vector2D p1 = new Vector2D(3, 1);
    Vector2D p2 = new Vector2D(5, 1);
    Circle2D circle = new Circle2D(4, 2, 1);

    assertTrue(circle.intersectsLine(p1, p2));
  }

}
