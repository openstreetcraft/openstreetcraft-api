// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

public class FilterTest {

  @Test
  public void filtersListByPredicate() {
    Iterable<String> list = ImmutableList.of("foo", "bar");
    Predicate<String> predicate = new Predicate<String>() {
      @Override
      public boolean apply(String input) {
        return input.equals("foo");
      }
    };
    Iterable<String> expected = ImmutableList.of("foo");
    Iterable<String> actual = Filter.filter(list, predicate);
    assertEquals(expected, actual);
  }

}
