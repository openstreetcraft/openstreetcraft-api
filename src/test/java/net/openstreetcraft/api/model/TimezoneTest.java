// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.model;

import junitx.extensions.EqualsHashCodeTestCase;

public class TimezoneTest extends EqualsHashCodeTestCase {

  public TimezoneTest() {
    super("timezoneTest");
  }

  @Override
  protected Object createInstance() throws Exception {
    return new Timezone(
        "2018-11-25T14:33+02:00[Africa/Tripoli]",
        "2018-11-25T08:50+02:00[Africa/Tripoli]",
        "2018-11-25T19:47+02:00[Africa/Tripoli]");
  }

  @Override
  protected Object createNotEqualInstance() throws Exception {
    return new Timezone(
        "2018-11-26T14:33+02:00[Africa/Tripoli]",
        "2018-11-26T08:50+02:00[Africa/Tripoli]",
        "2018-11-26T19:47+02:00[Africa/Tripoli]");
  }

}
