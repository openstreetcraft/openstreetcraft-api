// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import java.util.Arrays;
import java.util.Map;

import com.google.common.base.Optional;
import com.google.common.collect.Maps;

import de.ixilon.osm.schema.OsmNode;

public class OsmNodeList {
  private final Map<Long, OsmNode> nodes;

  private OsmNodeList(Map<Long, OsmNode> nodes) {
    this.nodes = nodes;
  }

  public static OsmNodeList of(OsmNode... nodes) {
    return copyOf(Arrays.asList(nodes));
  }

  public static OsmNodeList copyOf(Iterable<OsmNode> nodes) {
    return new OsmNodeList(Maps.uniqueIndex(nodes, OsmNode::getId));
  }

  public Optional<OsmNode> findNode(long id) {
    return Optional.fromNullable(nodes.get(id));
  }
}
