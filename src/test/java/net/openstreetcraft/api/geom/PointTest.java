// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import org.junit.Test;

import junitx.extensions.EqualsHashCodeTestCase;

public class PointTest extends EqualsHashCodeTestCase {

  public PointTest() {
    super("PointTest");
  }

  @Override
  protected Object createInstance() throws Exception {
    return new Point(1, 2);
  }

  @Override
  protected Object createNotEqualInstance() throws Exception {
    return new Point(3, 4);
  }

  @Test
  public void testAddsPoint() {
    Point actual = new Point(1, 2).add(new Point(3, 5));
    Point expected = new Point(4, 7);
    assertEquals(expected, actual);
  }

  @Test
  public void testSubstractsPoint() {
    Point actual = new Point(1, 2).sub(new Point(3, 5));
    Point expected = new Point(-2, -3);
    assertEquals(expected, actual);
  }

  @Test
  public void testMultipliesFactor() {
    Point actual = new Point(1, 2).mul(3);
    Point expected = new Point(3, 6);
    assertEquals(expected, actual);
  }

  @Test
  public void testDividesRatio() {
    Point actual = new Point(10, 20).div(5);
    Point expected = new Point(2, 4);
    assertEquals(expected, actual);
  }

}
