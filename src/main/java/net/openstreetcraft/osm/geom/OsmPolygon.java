// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;

import de.ixilon.osm.schema.OsmWay;

public class OsmPolygon extends OsmPolyline {
  
  private final Area polygon;

  /**
   * Line segments of the way forming a polygon. 
   */
  public OsmPolygon(OsmWay way, OsmNodeList nodes) {
    super(way, nodes);
    polygon = new Area(toPath2D());
  }

  /**
   * Returns true if the interior of the polygon intersects the interior of a bounding box.
   */
  @Override
  public boolean isInside(Rectangle bbox) {
    Rectangle2D rect = bbox.toRectangle2D();
    return polygon.intersects(rect);
  }
  
}
