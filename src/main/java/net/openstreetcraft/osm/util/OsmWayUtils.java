// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.util;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import de.ixilon.osm.schema.OsmNd;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmTag;
import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.osm.geom.OsmNodeList;

public final class OsmWayUtils {

  private static final Predicate<Object> IS_NODEREF = new Predicate<Object>() {
    @Override
    public boolean apply(Object input) {
      return input != null && OsmNd.class.isAssignableFrom(input.getClass());
    }
  };

  private static final Predicate<Object> IS_TAG = new Predicate<Object>() {
    @Override
    public boolean apply(Object input) {
      return input != null && OsmTag.class.isAssignableFrom(input.getClass());
    }
  };

  private OsmWayUtils() {
    // static utility class
  }
  
  /**
   * Returns true if the way is closed. 
   */
  public static boolean isClosed(OsmWay way) {
    List<OsmNd> refs = ImmutableList.copyOf(getNodeRefs(way));
    if (refs.isEmpty()) {
      return false;
    }
    int last = refs.size() - 1;
    return refs.get(0).getRef() == refs.get(last).getRef();
  }

  /**
   * Creates a new node list containing all nodes which are within the way and within the given node
   * list.
   */
  public static Iterable<OsmNode> getNodes(OsmWay way, OsmNodeList nodes) {
    ImmutableList.Builder<OsmNode> result = ImmutableList.builder();
    for (OsmNd nodeRef : getNodeRefs(way)) {
      Optional<OsmNode> node = nodes.findNode(nodeRef.getRef());
      if (node.isPresent()) {
        result.add(node.get());
      }
    }
    return result.build();
  }

  /**
   * Extract node references. 
   */
  public static Iterable<OsmNd> getNodeRefs(OsmWay way) {
    return Filter.filter(way.getTagOrNd(), IS_NODEREF);
  }

  /**
   * Extract tags. 
   */
  public static Iterable<OsmTag> getTags(OsmWay way) {
    return Filter.filter(way.getTagOrNd(), IS_TAG);
  }

  /**
   * Creates a pseudo random number generator initialized from the given way. 
   */
  public static Random createRandom(OsmWay way) {
    long seed = 0;
    try {
      seed = getNodeRefs(way).iterator().next().getRef();
    } catch (NoSuchElementException exception) {
      // use default
    }
    return new Random(seed);
  }

}
