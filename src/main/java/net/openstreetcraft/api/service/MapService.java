// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.ImmutableSet;

import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.model.Layer;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.minecraft.world.World;
import net.openstreetcraft.projection.SphericalLocation;

/**
 * Service facade of the Openstreetcraft map webservice.
 */
@Service
public class MapService {

  private final RestTemplate restTemplate;
  private final String endpoint;

  @Autowired
  public MapService(RestTemplate restTemplate, HttpHost gateway) {
    this.restTemplate = restTemplate;
    this.endpoint = gateway.toURI() + "/map";
  }

  /**
   * Get list of supported world maps.
   */
  public ImmutableSet<World> getWorlds() throws RestClientException {
    return ImmutableSet.of(World.EARTH);
  }

  /**
   * Get list of supported layers on a world map.
   */
  public ImmutableSet<Layer> getLayers(World world) throws RestClientException {
    ImmutableSet.Builder<Layer> layers = ImmutableSet.builder();
    Map<String, Object> variables = new HashMap<>();

    variables.put("map", world);

    String url = endpoint + "/maps/{map}/layers";

    for (Object layer : restTemplate.getForObject(url, Iterable.class, variables)) {
      try {
        layers.add(Layer.of((String) layer));
      } catch (IllegalArgumentException exception) {
        // layer is supported by backend, but unknown in the API
      }
    }

    return layers.build();
  }

  /**
   * Get byte data samples on a layer within a given bounding box. The returned
   * raster contains exactly width * height data samples or an exception is
   * thrown.
   */
  public Raster<Byte> getByteSamples(World world, Layer layer, BoundingBox<SphericalLocation> bbox,
      int width, int height) throws RestClientException {
    Map<String, Object> variables = new HashMap<>();

    variables.put("map", world);
    variables.put("layer", layer);
    // WMS 1.3.0 is using x=lat, y=lon
    variables.put("minx", bbox.getSo().getLatitude());
    variables.put("miny", bbox.getNw().getLongitude());
    variables.put("maxx", bbox.getNw().getLatitude());
    variables.put("maxy", bbox.getSo().getLongitude());
    variables.put("width", width);
    variables.put("height", height);

    String url = endpoint + "/maps/{map}/layers/{layer}/bytes?"
        + "minx={minx}&miny={miny}&maxx={maxx}&maxy={maxy}&width={width}&height={height}";

    byte[] samples = restTemplate.getForObject(url, byte[].class, variables);
    return new Raster<Byte>(width, height, toByteArray(samples));
  }

  private static Byte[] toByteArray(byte[] array) {
    final Byte[] result = new Byte[array.length];
    for (int i = 0; i < array.length; i++) {
      result[i] = Byte.valueOf(array[i]);
    }
    return result;
  }

  /**
   * Get float data samples on a layer within a given bounding box. The returned
   * raster contains exactly width * height data samples or an exception is
   * thrown.
   */
  public Raster<Float> getFloatSamples(World world, Layer layer,
      BoundingBox<SphericalLocation> bbox, int width, int height) throws RestClientException {
    Map<String, Object> variables = new HashMap<>();

    variables.put("map", world);
    variables.put("layer", layer);
    // WMS 1.3.0 is using x=lat, y=lon
    variables.put("minx", bbox.getSo().getLatitude());
    variables.put("miny", bbox.getNw().getLongitude());
    variables.put("maxx", bbox.getNw().getLatitude());
    variables.put("maxy", bbox.getSo().getLongitude());
    variables.put("width", width);
    variables.put("height", height);

    String url = endpoint + "/maps/{map}/layers/{layer}/floats?"
        + "minx={minx}&miny={miny}&maxx={maxx}&maxy={maxy}&width={width}&height={height}";

    byte[] samples = restTemplate.getForObject(url, byte[].class, variables);
    return new Raster<Float>(width, height, toFloatArray(samples));
  }

  private static Float[] toFloatArray(byte[] bytes) {
    ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
    Float[] result = new Float[bytes.length / Float.BYTES];
    for (int i = 0; i < result.length; i++) {
      result[i] = byteBuffer.getFloat(i * Float.BYTES);
    }
    return result;
  }
}
