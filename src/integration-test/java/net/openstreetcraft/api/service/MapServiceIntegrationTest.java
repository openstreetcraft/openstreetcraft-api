// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableSet;

import net.openstreetcraft.api.geom.Raster;
import net.openstreetcraft.api.model.Layer;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.minecraft.world.World;
import net.openstreetcraft.projection.SphericalLocation;

public class MapServiceIntegrationTest {

  private RestTemplateFactory restTemplateFactory = new RestTemplateFactory();
  private MapService mapService;

  @Before
  public void setUp() {
    mapService = new MapService(
        restTemplateFactory.getRestTemplate(),
        restTemplateFactory.getApiGateway());
  }

  @Test
  public void listsWorlds() {
    Set<World> actual = mapService.getWorlds();
    Set<World> expected = ImmutableSet.of(World.EARTH);
    assertEquals(expected, actual);
  }

  @Test
  public void getElevationSamples() {
    World world = World.EARTH;
    Layer layer = Layer.ELEVATION;

    BoundingBox<SphericalLocation> bbox = BoundingBox.of(
        SphericalLocation.of(-4.5703125, 54.3677585240684),
        SphericalLocation.of(-4.21875, 54.16243396806781));

    int width = 4;
    int height = 4;

    Raster<Float> actual = mapService.getFloatSamples(world, layer, bbox, width, height);

    Float[] data = new Float[] {
        74.0f, 140.0f, 60.0f, 8.0f,
        261.0f, 263.0f, 115.0f, 14.0f,
        309.0f, 214.0f, 138.0f, 7.0f,
        104.0f, 20.0f, 107.0f, -6.0f
    };
    
    Raster<Float> expected = new Raster<Float>(width, height, data);

    assertEquals(expected, actual);
  }

  @Test
  public void getBiomeSamples() {
    World world = World.EARTH;
    Layer layer = Layer.BIOME;

    BoundingBox<SphericalLocation> bbox = BoundingBox.of(
        SphericalLocation.of(-4.5703125, 54.3677585240684),
        SphericalLocation.of(-4.21875, 54.16243396806781));

    int width = 4;
    int height = 4;

    Raster<Byte> actual = mapService.getByteSamples(world, layer, bbox, width, height);

    Byte[] data = new Byte[] {
        1, 1, 0, 0,
        1, 1, 1, 0,
        1, 1, 0, 0,
        1, 1, 0, 0
    };
    
    Raster<Byte> expected = new Raster<Byte>(width, height, data);

    assertEquals(expected, actual);
  }

  @Test
  public void getPlasticSamples() {
    World world = World.EARTH;
    Layer layer = Layer.PLASTIC_C4;
    
    BoundingBox<SphericalLocation> bbox = BoundingBox.of(
        SphericalLocation.of(-160.1, 30.1),
        SphericalLocation.of(-160.0, 30.0));

    int width = 4;
    int height = 4;

    Raster<Byte> actual = mapService.getByteSamples(world, layer, bbox, width, height);

    Byte[] data = new Byte[] {
        2, 2, 2, 2,
        2, 2, 2, 2,
        2, 2, 2, 2,
        2, 2, 2, 2
    };
    
    Raster<Byte> expected = new Raster<Byte>(width, height, data);

    assertEquals(expected, actual);
  }
}
