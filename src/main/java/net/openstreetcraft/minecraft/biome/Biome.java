// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.biome;

/**
 * Common biomes of Java and Bedrock edition. 
 */
public enum Biome {
  BAMBOO_JUNGLE,
//BAMBOO_JUNGLE_HILLS,
  BASALT_DELTAS,
  BEACH,
  BIRCH_FOREST,
//BIRCH_FOREST_HILLS,
  COLD_OCEAN,
  CRIMSON_FOREST,
  DEEP_COLD_OCEAN,
  DEEP_FROZEN_OCEAN,
  DEEP_LUKEWARM_OCEAN,
  DEEP_OCEAN,
//DEEP_WARM_OCEAN,
  DESERT,
//DESERT_HILLS,
  FLOWER_FOREST,
  FOREST,
  FROZEN_OCEAN,
  FROZEN_RIVER,
  JUNGLE,
//JUNGLE_EDGE,
//JUNGLE_HILLS,
  LUKEWARM_OCEAN,
  OCEAN,
  PLAINS,
  RIVER,
  SAVANNA,
  SAVANNA_PLATEAU,
  SOUL_SAND_VALLEY,
  SUNFLOWER_PLAINS,
  SWAMP,
  TAIGA,
//TAIGA_HILLS,
  THE_END,
  WARM_OCEAN,
  WARPED_FOREST;
}
