// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CoverTest {
  private final Cover.Converter converter = new Cover.Converter();

  @Test
  public void convertsByteToCover() {
    assertEquals(Cover.URBAN, converter.apply((byte) 190));
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsOnUnknownSample() {
    converter.apply((byte) 255);
  }
}
