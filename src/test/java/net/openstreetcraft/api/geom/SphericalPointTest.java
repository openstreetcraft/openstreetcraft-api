// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import net.openstreetcraft.projection.SphericalLocation;

public class SphericalPointTest {

  @Test
  public void convertsSphericalLocationIntoPoint() {
    double latitude = 1.2;
    double longitude = 3.4;
    SphericalLocation location =
        SphericalLocation.builder().latitude(latitude).longitude(longitude).build();
    
    Point actual = new SphericalPoint(location);
    
    assertEquals(longitude, actual.getX(), Double.MIN_VALUE);
    assertEquals(latitude, actual.getY(), Double.MIN_VALUE);
  }

  @Test
  public void convertsPointIntoSphericalLocation() {
    double latitude = 1.2;
    double longitude = 3.4;
    SphericalLocation location =
        SphericalLocation.builder().latitude(latitude).longitude(longitude).build();
    
    SphericalPoint actual = new SphericalPoint(location);
    
    assertEquals(location, actual.toSphericalLocation());
  }

}
