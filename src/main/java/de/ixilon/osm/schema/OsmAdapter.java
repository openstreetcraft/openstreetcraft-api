// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package de.ixilon.osm.schema;

import java.util.List;

/**
 * Adapter to be compatible with overpass-api 0.x clients.
 */
public class OsmAdapter implements Osm {
  private final OsmRoot delegate;

  public OsmAdapter(OsmRoot delegate) {
    this.delegate = delegate;
  }

  @Override
  public OsmBound getBound() {
    return delegate.getBound();
  }

  @Override
  public List<Object> getNodesAndRelationsAndWaies() {
    return delegate.getNodeOrRelationOrWay();
  }
}
