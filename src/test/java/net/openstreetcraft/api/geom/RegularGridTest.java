// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.google.common.collect.ImmutableList;

public class RegularGridTest {

  @Test
  public void iteratesOverIrregularSquare() {
    BoundingBox bbox = new BoundingBox.Builder()
        .withLowerLeft(new Point(1, 2))
        .withUpperLeft(new Point(1, 4))
        .withLowerRight(new Point(5, 0))
        .withUpperRight(new Point(3, 5))
        .build();
    RegularGrid grid = new RegularGrid(bbox, 3, 3);
    List<Point> actual = ImmutableList.copyOf(grid.iterator());
    List<Point> expected = ImmutableList.of(
        new Point(1, 2),
        new Point(3, 1),
        new Point(5, 0),

        new Point(1, 3),
        new Point(2.5, 2.75),
        new Point(4, 2.5),

        new Point(1, 4),
        new Point(2, 4.5),
        new Point(3, 5));
    assertEquals(expected, actual);
  }

}
