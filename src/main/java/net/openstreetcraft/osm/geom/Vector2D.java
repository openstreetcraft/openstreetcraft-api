// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import java.awt.geom.Point2D;

public class Vector2D {

  private final double x;
  private final double y;

  public Vector2D(Point2D point) {
    this(point.getX(), point.getY());
  }
  
  public Vector2D(double x, double y) {
    this.x = x;
    this.y = y;
  }

  public Vector2D prod(double factor) {
    return new Vector2D(x * factor, y * factor);
  }

  public Vector2D sum(Vector2D other) {
    return new Vector2D(x + other.x, y + other.y);
  }

  public Vector2D diff(Vector2D other) {
    return new Vector2D(x - other.x, y - other.y);
  }

  public double dot(Vector2D other) {
    return x * other.x + y * other.y;
  }

  public double length() {
    return Math.sqrt(x * x + y * y);
  }

  public Point2D toPoint() {
    return new Point2D.Double(x, y);
  }
}
