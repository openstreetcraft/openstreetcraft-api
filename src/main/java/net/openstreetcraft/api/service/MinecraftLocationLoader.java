// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import java.util.List;
import java.util.Map;

import net.openstreetcraft.coordinates.BlockLocation;
import net.openstreetcraft.minecraft.world.Partition;
import net.openstreetcraft.projection.SphericalLocation;

public class MinecraftLocationLoader extends AbstractLoader<SphericalLocation, BlockLocation> {
  private final CoordinatesService service;
  private final Partition partition;

  public MinecraftLocationLoader(CoordinatesService service, Partition partition) {
    this.service = service;
    this.partition = partition;
  }

  @Override
  protected Map<SphericalLocation, BlockLocation> fetchAll(List<SphericalLocation> keys) {
    return combine(keys, service.getMinecraftLocations(keys, partition));
  }

  @Override
  protected BlockLocation fetch(SphericalLocation key) {
    return service.getMinecraftLocation(key, partition);
  }
}
