// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import java.util.Arrays;
import java.util.Objects;
import java.util.function.Function;

public class Raster<T> {
  private final int width;
  private final int height;
  private final T[] data;

  /**
   * Data array representing grid of width * height data samples.
   */
  @SafeVarargs
  public Raster(int width, int height, T... data) {
    assert (data.length == width * height);
    this.width = width;
    this.height = height;
    this.data = data.clone();
  }

  /**
   * Convert into raster of type R by using a converter function.
   */
  public <R> Raster<R> convert(Function<T, R> converter) {
    @SuppressWarnings("unchecked")
    R[] dest = (R[]) new Object[width * height];

    for (int index = 0; index < dest.length; index++) {
      dest[index] = converter.apply(data[index]);
    }

    return new Raster<R>(width, height, dest);
  }

  @Override
  public int hashCode() {
    return Objects.hash(width, height, data);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Raster)) {
      return false;
    }

    @SuppressWarnings("unchecked")
    Raster<T> other = (Raster<T>) obj;

    return Objects.equals(this.width, other.width)
        && Objects.equals(this.height, other.height)
        && Arrays.equals(this.data, other.data);
  }

  @Override
  public String toString() {
    return "Raster [width=" + width + ", height=" + height + ", data=" + Arrays.toString(data)
        + "]";
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  /**
   * Data sample at a specified coordinate.
   */
  public T getSample(int x, int y) {
    require(x >= 0 && x < width);
    require(y >= 0 && y < height);
    return data[x + y * width];
  }

  private static void require(boolean requirement) {
    if (!requirement) {
      throw new ArrayIndexOutOfBoundsException();
    }
  }

}
