// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import net.openstreetcraft.api.model.Timezone;
import net.openstreetcraft.api.rest.RestTemplateFactory;
import net.openstreetcraft.projection.SphericalLocation;

public class TimezoneServiceIntegrationTest {

  private RestTemplateFactory restTemplateFactory = new RestTemplateFactory();
  private TimezoneService timezoneService;

  @Before
  public void setUp() {
    timezoneService = new TimezoneService(
        restTemplateFactory.getRestTemplate(),
        restTemplateFactory.getApiGateway());
  }

  @Test
  public void getsTimezoneByLocation() {
    SphericalLocation location = SphericalLocation.of(-122.0856086, 37.4224082);
    Timezone actual = timezoneService.getTimezone(location);
    assertNotNull(actual);
  }

}
