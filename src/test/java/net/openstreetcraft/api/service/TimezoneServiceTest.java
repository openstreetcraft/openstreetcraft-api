// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

import org.apache.http.HttpHost;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.api.model.Timezone;
import net.openstreetcraft.projection.SphericalLocation;

public class TimezoneServiceTest {

  private static final HttpHost GATEWAY = new HttpHost("gateway");
  private static final String ENDPOINT = "http://gateway/timezone";

  private MockRestServiceServer mockServer;
  private TimezoneService service;

  @Before
  public void setUp() {
    RestTemplate restTemplate = new RestTemplate();

    mockServer = MockRestServiceServer.createServer(restTemplate);
    service = new TimezoneService(restTemplate, GATEWAY);
  }

  @Test
  public void getsTimezoneAtLocation() {
    mockServer.expect(requestTo(ENDPOINT + "/timezone?longitude=3.4&latitude=1.2"))
        .andRespond(
            withSuccess("{\"currentTime\": \"2018-11-25T14:33+02:00[Africa/Tripoli]\","
                + "\"sunrise\": \"2018-11-25T08:50+02:00[Africa/Tripoli]\","
                + "\"sunset\": \"2018-11-25T19:47+02:00[Africa/Tripoli]\"}",
                MediaType.APPLICATION_JSON));

    Timezone expected = new Timezone(
        "2018-11-25T14:33+02:00[Africa/Tripoli]",
        "2018-11-25T08:50+02:00[Africa/Tripoli]",
        "2018-11-25T19:47+02:00[Africa/Tripoli]");
    
    Timezone actual = service.getTimezone(SphericalLocation.of(3.4, 1.2));

    mockServer.verify();
    assertEquals(expected, actual);
  }

}
