// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import java.util.Objects;

public class Point {
  
  private final double x;
  private final double y;

  public Point(double x, double y) {
    this.x = x;
    this.y = y;
  }
  
  public Point add(Point summand) {
    return new Point(x + summand.x, y + summand.y);
  }

  public Point sub(Point subtrahend) {
    return new Point(x - subtrahend.x, y - subtrahend.y);
  }

  public Point mul(double factor) {
    return new Point(x * factor, y * factor);
  }

  public Point div(double ratio) {
    return new Point(x / ratio, y / ratio);
  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Point)) {
      return false;
    }

    Point other = (Point) obj;

    return Objects.equals(this.x, other.x)
        && Objects.equals(this.y, other.y);
  }

  @Override
  public String toString() {
    return "Point [x=" + x + ", y=" + y + "]";
  }
  
}
