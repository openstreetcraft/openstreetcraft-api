// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.api.model.Weather;
import net.openstreetcraft.projection.SphericalLocation;

/**
 * Service facade of the Openstreetcraft weather webservice.
 */
@Service
public class WeatherService {

  private final RestTemplate restTemplate;
  private final String endpoint;

  @Autowired
  public WeatherService(RestTemplate restTemplate, HttpHost gateway) {
    this.restTemplate = restTemplate;
    this.endpoint = gateway.toURI() + "/weather";
  }

  /**
   * Returns weather conditions at a location.
   */
  public Weather getWeather(SphericalLocation currentLocation) throws RestClientException {
    Map<String, Object> variables = new HashMap<>();

    variables.put("longitude", currentLocation.getLongitude());
    variables.put("latitude", currentLocation.getLatitude());

    return restTemplate.getForObject(
        endpoint + "/weather?longitude={longitude}&latitude={latitude}",
        Weather.class, variables);
  }

}
