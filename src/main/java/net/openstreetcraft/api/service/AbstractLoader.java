// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class AbstractLoader<K, V> implements Loader<K, V> {

  private final List<K> keys = new ArrayList<>();
  private Map<K, V> cache = new HashMap<>();

  @Override
  public synchronized void invalidate() {
    keys.clear();
    cache.clear();
  }

  @Override
  public synchronized void prefetch(K key) {
    keys.add(key);
  }

  @Override
  public synchronized V get(K key) {
    if (!keys.isEmpty() && !cache.containsKey(key)) {
      cache = fetchAll(keys);
    }
    return cache.computeIfAbsent(key, k -> fetch(k));
  }

  protected Map<K, V> fetchAll(List<K> keys) {
    Map<K, V> result = new HashMap<>();
    for (K key : keys) {
      result.put(key, fetch(key));
    }
    return result;
  }

  protected abstract V fetch(K key);
  
  protected static final <K, V> Map<K, V> combine(List<K> keys, List<V> values) {
    Iterator<K> keyIterator = keys.iterator();
    Iterator<V> valueIterator = values.iterator();
    Map<K, V> result = new HashMap<>();
    while (keyIterator.hasNext() || valueIterator.hasNext()) {
      result.put(keyIterator.next(), valueIterator.next());
    }
    return result;
  }
}
