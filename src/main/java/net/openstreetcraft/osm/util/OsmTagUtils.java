// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.util;

import com.google.common.base.Optional;

import de.ixilon.osm.schema.OsmTag;

public final class OsmTagUtils {

  private OsmTagUtils() {
    // static utility class
  }

  /**
   * Find the tag matching a given name and convert the result to an enum.  
   */
  public static <E extends Enum<E>> Optional<E> getTag(Iterable<OsmTag> tags, String key,
      Class<E> clazz) {
    Optional<String> text = getTag(tags, key);
    if (text.isPresent()) {
      try {
        return Optional.of(Enum.valueOf(clazz, text.get().toUpperCase().replace(':', '_')));
      } catch (IllegalArgumentException exception) {
        // unknown
      }
    }
    return Optional.absent();
  }

  /**
   * Find the tag matching a given name.  
   */
  public static Optional<String> getTag(Iterable<OsmTag> tags, String key) {
    for (OsmTag tag : tags) {
      if (key.equals(tag.getK())) {
        return Optional.of(tag.getV());
      }
    }
    return Optional.absent();
  }

  /**
   * Returns true if the collection contains a tag with this name.   
   */
  public static boolean isTag(Iterable<OsmTag> tags, String key) {
    for (OsmTag tag : tags) {
      if (key.equals(tag.getK())) {
        return true;
      }
    }
    return false;
  }

  /**
   * Returns true if the collection contains a tag with this name and value.   
   */
  public static boolean isTag(Iterable<OsmTag> tags, String key, String value) {
    for (OsmTag tag : tags) {
      if (key.equals(tag.getK()) && value.equals(tag.getV())) {
        return true;
      }
    }
    return false;
  }

}
