// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.rest.apache;

import java.util.concurrent.TimeUnit;

import org.apache.http.HttpHost;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class HttpClientConfiguration {

  private static final int MAX_TOTAL_CONNECTIONS = 10;
  private static final int SOCKET_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(60);
  private static final int CONNECT_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(30);
  private static final int REQUEST_TIMEOUT = (int) TimeUnit.SECONDS.toMillis(30);

  @Value("${openstreetcraft.protocol}")
  private String protocol;

  @Value("${openstreetcraft.host}")
  private String host;

  @Value("${openstreetcraft.port}")
  private String port;

  /**
   * Caching HTTP client.
   */
  @Bean
  public HttpClient getHttpClient(CredentialsProvider credentialsProvider) {
    return CachingHttpClientBuilder.create()
        .setCacheConfig(cacheConfig())
        // .setProxy(getHttpHost())
        .setDefaultCredentialsProvider(credentialsProvider)
        .setDefaultRequestConfig(requestConfig())
        .setConnectionManager(connectionManager())
        .build();
  }

  private static CacheConfig cacheConfig() {
    return CacheConfig.custom()
        .setMaxCacheEntries(1 * 1024)
        .setMaxObjectSize(100 * 1024)
        .setNeverCacheHTTP10ResponsesWithQueryString(false)
        .build();
  }

  /**
   * URL of the API gateway.
   */
  @Bean
  public HttpHost getHttpHost() {
    return new HttpHost(host, Integer.parseInt(port), protocol);
  }

  private static RequestConfig requestConfig() {
    return RequestConfig.custom()
        .setConnectionRequestTimeout(REQUEST_TIMEOUT)
        .setConnectTimeout(CONNECT_TIMEOUT)
        .setSocketTimeout(SOCKET_TIMEOUT)
        .build();
  }

  private static HttpClientConnectionManager connectionManager() {
    PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
    connectionManager.setMaxTotal(MAX_TOTAL_CONNECTIONS);
    return connectionManager;    
  }
}
