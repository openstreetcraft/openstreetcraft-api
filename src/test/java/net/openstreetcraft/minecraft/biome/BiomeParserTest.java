// Copyright (C) 2020 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.biome;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BiomeParserTest {
  @Test
  public void parsesJavaBiomes() {
    BiomeParser<JavaBiome> parser = new BiomeParser<>(JavaBiome.class);
    assertEquals(Biome.OCEAN, parser.parse(0));
  }

  @Test
  public void parsesBedrockBiomes() {
    BiomeParser<BedrockBiome> parser = new BiomeParser<>(BedrockBiome.class);
    assertEquals(Biome.OCEAN, parser.parse(42));
  }

  @Test
  public void identifiesJavaBiomes() {
    BiomeParser<JavaBiome> parser = new BiomeParser<>(JavaBiome.class);
    assertEquals(0, parser.identifiable(Biome.OCEAN).ident());
  }

  @Test
  public void identifiesBedrockBiomes() {
    BiomeParser<BedrockBiome> parser = new BiomeParser<>(BedrockBiome.class);
    assertEquals(42, parser.identifiable(Biome.OCEAN).ident());
  }
}
