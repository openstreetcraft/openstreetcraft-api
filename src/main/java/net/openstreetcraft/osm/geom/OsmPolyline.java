// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmWay;
import net.openstreetcraft.osm.util.OsmWayUtils;

public class OsmPolyline {

  private final OsmWay way;
  private final Path2D path;
  private final List<Line2D> lines;

  private Line2D line;

  /**
   * Line segments of the way.
   */
  public OsmPolyline(OsmWay way, OsmNodeList nodes) {
    this.way = way;

    path = new Path2D.Double();
    lines = new ArrayList<>();

    Point2D p1 = null;
    Point2D p2;

    for (OsmNode node : OsmWayUtils.getNodes(way, nodes)) {
      Point2D point = new OsmPoint(node).toPoint2D();
      if (p1 == null) {
        path.moveTo(point.getX(), point.getY());
        p1 = point;
      } else {
        path.lineTo(point.getX(), point.getY());
        p2 = point;
        lines.add(new Line2D.Double(p1, p2));
        p1 = p2;
      }
    }
  }

  /**
   * Returns true if any line segment intersects the interior of a bounding box.
   */
  public boolean isInside(Rectangle bbox) {
    Rectangle2D rect = bbox.toRectangle2D();
    for (Line2D line : lines) {
      if (rect.intersectsLine(line)) {
        this.line = line;
        return true;
      }
    }
    return false;
  }

  /**
   * Returns true if any line segment intersects the interior of a circle.
   */
  public boolean isInside(Circle2D circle) {
    for (Line2D line : lines) {
      if (circle.intersectsLine(line.getX1(), line.getY1(), line.getX2(), line.getY2())) {
        this.line = line;
        return true;
      }
    }
    return false;
  }

  /**
   * Shortest distance between intersection line segment and center of bounding box. The
   * intersection line segment is a side effect of isInside().
   */
  public final double distance(Rectangle bbox) {
    return distance(
        new Point2D.Double(bbox.toRectangle2D().getCenterX(), bbox.toRectangle2D().getCenterY()));
  }

  /**
   * Shortest distance between intersection line segment and the given point. The intersection line
   * segment is a side effect of isInside().
   */
  public final double distance(Point2D point) {
    return line.ptLineDist(point);
  }

  public OsmWay toWay() {
    return way;
  }

  public Path2D toPath2D() {
    return path;
  }

}
