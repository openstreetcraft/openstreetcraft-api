// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LayerTest {

  @Test
  public void parsesBiomeLayer() {
    assertEquals("BIOME", Layer.of("Biome").toString());
  }

  @Test
  public void parsesElevationLayer() {
    assertEquals("ELEVATION", Layer.of("Elevation").toString());
  }

  @Test
  public void parsesPlasticLayers() {
    assertEquals("PLASTIC_C1", Layer.of("plastic_c1").toString());
    assertEquals("PLASTIC_C2", Layer.of("plastic_c2").toString());
    assertEquals("PLASTIC_C3", Layer.of("plastic_c3").toString());
    assertEquals("PLASTIC_C4", Layer.of("plastic_c4").toString());
  }

  @Test(expected = IllegalArgumentException.class)
  public void throwsUnknownLayer() {
    Layer.of("unknown");
  }

}
