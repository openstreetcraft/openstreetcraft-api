// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.geom.Point2D;

import org.junit.Before;
import org.junit.Test;

import de.ixilon.osm.schema.OsmNode;

public class OsmPointTest {

  private static final double LAT = 1.0;
  private static final double LON = 2.0;

  private OsmPoint osmPoint;

  @Before
  public void setUp() {
    OsmNode node = new OsmNode();
    node.setLat(LAT);
    node.setLon(LON);
    osmPoint = new OsmPoint(node);
  }

  @Test
  public void convertsOsmNodeToPoint2D() {
    Point2D expected = new Point2D.Double(2.0, 1.0);
    Point2D actual = osmPoint.toPoint2D();
    assertEquals(expected, actual);
  }

  @Test
  public void ignoresPointOutsideOfBoundingBox() {
    Rectangle bbox = Rectangle.builder().xmin(3.0).ymin(3.0).xmax(4.0).ymax(4.0).build();
    assertFalse(osmPoint.isInside(bbox));
  }

  @Test
  public void findsPointInsideOfBoundingBox() {
    Rectangle bbox = Rectangle.builder().xmin(1.0).ymin(1.0).xmax(4.0).ymax(4.0).build();
    assertTrue(osmPoint.isInside(bbox));
  }

  @Test
  public void findsPointOnLeftBoundaryOfBoundingBox() {
    Rectangle bbox = Rectangle.builder().xmin(2.0).ymin(1.0).xmax(4.0).ymax(4.0).build();
    assertTrue(osmPoint.isInside(bbox));
  }

  @Test
  public void ignoresPointOnRightBoundaryOfBoundingBox() {
    Rectangle bbox = Rectangle.builder().xmin(0.0).ymin(0.0).xmax(2.0).ymax(1.0).build();
    assertFalse(osmPoint.isInside(bbox));
  }

}
