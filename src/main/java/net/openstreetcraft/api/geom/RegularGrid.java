// Copyright (C) 2019 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.geom;

import java.util.Iterator;
import java.util.Objects;

public class RegularGrid implements Iterable<Point> {

  private final BoundingBox bbox;
  private final int width;
  private final int height;

  /**
   * A width * height grid on an irregular square.
   */
  public RegularGrid(BoundingBox bbox, int width, int height) {
    this.bbox = bbox;
    this.width = width;
    this.height = height;
  }

  @Override
  public int hashCode() {
    return Objects.hash(bbox, width, height);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof RegularGrid)) {
      return false;
    }

    RegularGrid other = (RegularGrid) obj;

    return Objects.equals(this.bbox, other.bbox) 
        && Objects.equals(this.width, other.width)
        && Objects.equals(this.height, other.height);
  }

  @Override
  public String toString() {
    return "RegularGrid [bbox=" + bbox + ", width=" + width + ", height=" + height + "]";
  }

  @Override
  public Iterator<Point> iterator() {
    return new Iterator<Point>() {

      private int x = 0;
      private int y = 0;
      private Point leftEnd = bbox.getLowerLeft();
      private Point rightEnd = bbox.getLowerRight();

      @Override
      public boolean hasNext() {
        return x < width && y < height;
      }

      @Override
      public Point next() {
        double dx = div(x, width - 1);
        Point point = dividingPoint(leftEnd, rightEnd, dx);
        increment();
        return point;
      }

      private double div(int dividend, int divisor) {
        return divisor == 0 ? 0.5 : (double) dividend / (double) divisor;
      }

      private Point dividingPoint(Point start, Point end, double factor) {
        return end.sub(start).mul(factor).add(start);
      }

      private void increment() {
        x += 1;
        if (x >= width) {
          x = 0;
          y += 1;
          double dy = div(y, height - 1);
          leftEnd = dividingPoint(bbox.getLowerLeft(), bbox.getUpperLeft(), dy);
          rightEnd = dividingPoint(bbox.getLowerRight(), bbox.getUpperRight(), dy);
        }
      }
    };
  }

}
