// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.model;

import java.util.function.Function;

/**
 * ESA Globcover data values.
 */
public enum Cover {
  /** Post-flooding or irrigated croplands (or aquatic). */
  IRRIGATED_CROPLANDS(11),

  /** Rainfed croplands. */
  RAINFED_CROPLANDS(14),

  /** Mosaic cropland (50-70%) / vegetation (grassland/shrubland/forest) (20-50%). */
  MOSAIC_CROPLANDS(20),

  /** Mosaic vegetation (grassland/shrubland/forest) (50-70%) / cropland (20-50%). */
  MOSAIC_VEGETATION(30),

  /** Closed to open (>15%) broadleaved evergreen or semi-deciduous forest (>5m). */
  BROADLEAVED_EVERGREEN_FOREST(40),

  /** Closed (>40%) broadleaved deciduous forest (>5m). */
  BROADLEAVED_DECIDUOUS_FOREST(50),

  /** Open (15-40%) broadleaved deciduous forest/woodland (>5m). */
  BROADLEAVED_WOODLAND(60),

  /** Closed (>40%) needleleaved evergreen forest (>5m). */
  NEEDLELEAVED_EVERGREEN_FOREST(70),

  /** Open (15-40%) needleleaved deciduous or evergreen forest (>5m). */
  NEEDLELEAVED_DECIDUOUS_FOREST(90),

  /** Closed to open (>15%) mixed broadleaved and needleleaved forest (>5m). */
  MIXED_FOREST(100),

  /** Mosaic forest or shrubland (50-70%) / grassland (20-50%). */
  MOSAIC_FOREST(110),

  /** Mosaic grassland (50-70%) / forest or shrubland (20-50%). */
  MOSAIC_GRASSLAND(120),

  /**
   * Closed to open (>15%) (broadleaved or needleleaved, evergreen or deciduous) shrubland (<5m).
   */
  SHRUBLAND(130),

  /** Closed to open (>15%) herbaceous vegetation (grassland, savannas or lichens/mosses). */
  HERBACEOUS_VEGETATION(140),

  /** Sparse (<15%) vegetation. */
  SPARSE_VEGETATION(150),

  /**
   * Closed to open (>15%) broadleaved forest regularly flooded (semi-permanently or temporarily) -
   * Fresh or brackish water.
   */
  REGULARLY_FLOODED_FOREST(160),

  /**
   * Closed (>40%) broadleaved forest or shrubland permanently flooded - Saline or brackish water.
   */
  PERMANENTLY_FLOODED_FOREST(170),

  /**
   * Closed to open (>15%) grassland or woody vegetation on regularly flooded or waterlogged soil -
   * Fresh, brackish or saline water.
   */
  REGULARLY_FLOODED_VEGETATION(180),

  /** Artificial surfaces and associated areas (Urban areas >50%). */
  URBAN(190),

  /** Bare areas. */
  BARE(200),

  /** Water bodies. */
  WATER(210),

  /** Permanent snow and ice. */
  ICE(220),

  /** No data (burnt areas, clouds, …). */
  NO_DATA(230);

  private int sample;

  private Cover(int sample) {
    this.sample = sample;
  }

  public static class Converter implements Function<Byte, Cover> {
    @Override
    public Cover apply(Byte sample) {
      return Cover.of(sample);
    }
  }

  private static Cover of(Byte sample) {
    for (Cover cover : values()) {
      if (cover.sample == Byte.toUnsignedInt(sample)) {
        return cover;
      }
    }
    throw new IllegalArgumentException(sample.toString());
  }
}
