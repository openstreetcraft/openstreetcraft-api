// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.rest.apache;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class CredentialsProviderConfiguration {

  @Value("${openstreetcraft.host}")
  private String host;

  @Value("${openstreetcraft.port}")
  private String port;

  @Value("${openstreetcraft.username:}")
  private String username;

  @Value("${openstreetcraft.password:}")
  private String password;

  /**
   * Create credentials provider.
   */
  @Bean
  public CredentialsProvider getCredentialsProvider() {
    CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
    credentialsProvider.setCredentials(getAuthScope(), getCredentials());
    return credentialsProvider;
  }
  
  private AuthScope getAuthScope() {
    return new AuthScope(host, Integer.parseInt(port));
  }
  
  private Credentials getCredentials() {
    return new UsernamePasswordCredentials(username, password);
  }
  
}
