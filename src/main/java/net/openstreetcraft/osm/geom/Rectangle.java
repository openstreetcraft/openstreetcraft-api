// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.geom;

import java.awt.geom.Rectangle2D;

public class Rectangle {

  private final Rectangle2D rect;

  private Rectangle(double xmin, double ymin, double xmax, double ymax) {
    rect = new Rectangle2D.Double(xmin, ymin, xmax - xmin, ymax - ymin);
  }

  Rectangle2D toRectangle2D() {
    return rect;
  }
  
  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private double xmin;
    private double ymin;
    private double xmax;
    private double ymax;
    
    public Rectangle build() {
      return new Rectangle(xmin, ymin, xmax, ymax);
    }

    public Builder xmin(double xmin) {
      this.xmin = xmin;
      return this;
    }

    public Builder ymin(double ymin) {
      this.ymin = ymin;
      return this;
    }

    public Builder xmax(double xmax) {
      this.xmax = xmax;
      return this;
    }

    public Builder ymax(double ymax) {
      this.ymax = ymax;
      return this;
    }
    
  }
  
}
