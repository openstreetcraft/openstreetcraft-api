// Copyright (C) 2024 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.world;

import java.util.Optional;

import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.projection.ProjectedLocation;
import net.openstreetcraft.tms.model.Tile;

public class TestPartitionService extends PartitionService {
  private final Tile tile;
  private final BoundingBox<ProjectedLocation> bounds;
  private final double minHeight;
  private final double maxHeight;

  /**
   * Test double to create a partition of the entire world.
   */
  public TestPartitionService() {
    this(new Tile(0, 0, 0),
        BoundingBox.of(
            ProjectedLocation.of(-20037508.3428, 10018754.1714),
            ProjectedLocation.of(20037508.3428, -10018754.1714)),
        10.0,
        20.0);
  }
  
  /**
   * Test double to create a partition with the given parameters.
   */
  public TestPartitionService(Tile tile, BoundingBox<ProjectedLocation> bounds, double minHeight,
      double maxHeight) {
    super(null, null);
    this.tile = tile;
    this.bounds = bounds;
    this.minHeight = minHeight;
    this.maxHeight = maxHeight;
  }

  public Optional<Partition> findPartitionAtLocation(ProjectedLocation location) {
    return Optional.of(new Partition(tile, bounds, minHeight, maxHeight));
  }
}
