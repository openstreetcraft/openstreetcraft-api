// Copyright (C) 2021 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import de.ixilon.osm.schema.OsmRoot;

@Service
public class CachingOverpassService extends OverpassService {

  private final Cache<String, OsmRoot> xapiCache;
  private final Cache<String, OsmRoot> xmlCache;

  /**
   * Caching results of overpass service.  
   */
  @Autowired
  public CachingOverpassService(RestTemplate restTemplate, HttpHost gateway,
      @Value("${overpass.cache:1}") int cacheSize) {
    super(restTemplate, gateway);
    xapiCache = new Cache<>(cacheSize);
    xmlCache = new Cache<>(cacheSize);
  }

  @Override
  protected synchronized OsmRoot getOsmRoot(String query) {
    return xapiCache.computeIfAbsent(query, key -> super.getOsmRoot(key));
  }  

  @Override
  protected synchronized OsmRoot postOsmRoot(String query) {
    return xmlCache.computeIfAbsent(query, key -> super.postOsmRoot(key));
  }  
}
