// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.http.HttpHost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import net.openstreetcraft.projection.SphericalLocation;

/**
 * Service facade of the Openstreetcraft location webservice.
 */
@Service
public class LocationService {

  private final RestTemplate restTemplate;
  private final String endpoint;

  @Autowired
  public LocationService(RestTemplate restTemplate, HttpHost gateway) {
    this.restTemplate = restTemplate;
    this.endpoint = gateway.toURI() + "/location";
  }

  /**
   * Returns location of a postal address.
   */
  public SphericalLocation getLocation(String address) throws RestClientException {
    Map<String, Object> variables = new HashMap<>();

    variables.put("address", address);

    return restTemplate.getForObject(endpoint + "/location?address={address}",
        SphericalLocation.class, variables);
  }

  /**
   * Returns location of a postal address near by current location.
   */
  public SphericalLocation getLocation(String address, SphericalLocation currentLocation)
      throws RestClientException {
    Map<String, Object> variables = new HashMap<>();

    variables.put("address", address);
    variables.put("longitude", stripDigits(currentLocation.getLongitude()));
    variables.put("latitude", stripDigits(currentLocation.getLatitude()));

    return restTemplate.getForObject(
        endpoint + "/location?address={address}&longitude={longitude}&latitude={latitude}",
        SphericalLocation.class, variables);
  }

  /**
   * Returns postal address of a location.
   */
  public String getAddress(SphericalLocation location) throws RestClientException {
    Map<String, Object> variables = new HashMap<>();

    variables.put("longitude", location.getLongitude());
    variables.put("latitude", location.getLatitude());

    return restTemplate.getForObject(
        endpoint + "/address?longitude={longitude}&latitude={latitude}", String.class, variables);
  }
  
  private static String stripDigits(double value) {
    return String.format(Locale.US, "%.5f", value);
  }

}
