// Copyright (C) 2017 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.osm.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.InputStream;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;

import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.ImmutableList;

import de.ixilon.osm.schema.ObjectFactory;
import de.ixilon.osm.schema.Osm;
import de.ixilon.osm.schema.OsmAdapter;
import de.ixilon.osm.schema.OsmNd;
import de.ixilon.osm.schema.OsmNode;
import de.ixilon.osm.schema.OsmRoot;
import de.ixilon.osm.schema.OsmTag;
import de.ixilon.osm.schema.OsmWay;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.Unmarshaller;
import net.openstreetcraft.osm.geom.OsmNodeList;

public class OsmWayUtilsTest {

  private OsmWay way;
  private OsmNodeList nodes;

  @Before
  public void setUp() throws Exception {
    JAXBContext context = JAXBContextFactory.createContext(new Class[] {ObjectFactory.class}, null);
    Unmarshaller unmarshaller = context.createUnmarshaller();
    try (InputStream resourceAsStream = getClass().getResourceAsStream("osm.xml")) {
      XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
      XMLStreamReader xmlStreamReader = xmlInputFactory.createXMLStreamReader(resourceAsStream);
      @SuppressWarnings("unchecked")
      JAXBElement<OsmRoot> response =
          (JAXBElement<OsmRoot>) unmarshaller.unmarshal(xmlStreamReader);
      Osm osm = new OsmAdapter(response.getValue());
      way = OsmUtils.getWays(osm).iterator().next();
      nodes = OsmNodeList.copyOf(OsmUtils.getNodes(osm));
    }
  }

  @Test
  public void isClosedWay() {
    assertTrue(OsmWayUtils.isClosed(way));
  }

  @Test
  public void getsTagsOfWay() {
    OsmTag tag = OsmWayUtils.getTags(way).iterator().next();

    assertEquals("building", tag.getK());
    assertEquals("yes", tag.getV());
  }

  @Test
  public void getsNodeRefsOfWay() {
    List<OsmNd> actual = ImmutableList.copyOf(OsmWayUtils.getNodeRefs(way));

    assertEquals(7, actual.size());

    OsmNd noderef = actual.get(0);

    assertEquals(4217868255L, noderef.getRef());
  }

  @Test
  public void getsNodesOfWay() {
    List<OsmNode> actual = ImmutableList.copyOf(OsmWayUtils.getNodes(way, nodes));

    assertEquals(6, actual.size());

    OsmNode node = actual.get(0);

    assertEquals(4217868255L, node.getId());
    assertEquals(48.2649760, node.getLat(), Double.MIN_VALUE);
    assertEquals(11.4346980, node.getLon(), Double.MIN_VALUE);
  }

}
