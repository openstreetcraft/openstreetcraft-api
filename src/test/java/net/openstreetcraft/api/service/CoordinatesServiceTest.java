// Copyright (C) 2018 Gerald Fiedler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.api.service;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.google.common.collect.ImmutableList;

import net.openstreetcraft.coordinates.BlockLocation;
import net.openstreetcraft.minecraft.world.Partition;
import net.openstreetcraft.minecraft.world.PartitionService;
import net.openstreetcraft.minecraft.world.TestPartitionService;
import net.openstreetcraft.projection.SphericalLocation;

public class CoordinatesServiceTest {

  private PartitionService partitionService;
  private Partition partition;
  private CoordinatesService service;
  
  @Before
  public void setUp() throws IOException {
    RestTemplate restTemplate = new RestTemplate();
    service = new CoordinatesService(restTemplate);
    partitionService = new TestPartitionService();
    partition = partitionService.findPartitionAtLocation(null).get();
  }

  @Test
  public void getsSphericalLocationOfMinecraftLocation() {
    BlockLocation location = BlockLocation.of(3339584.0, -6679169.0);
    SphericalLocation expected = SphericalLocation.of(29.999993498010078, 59.99999597917299);
    SphericalLocation actual = service.getSphericalLocation(location, partition);
    assertEquals(expected, actual);
  }

  @Test
  public void getsSphericalLocationsOfMinecraftLocations() {
    List<BlockLocation> locations = ImmutableList.of(
        BlockLocation.of(0.0, 0.0),
        BlockLocation.of(3339584.0, -6679169.0));
    List<SphericalLocation> expecteds = ImmutableList.of(
        SphericalLocation.of(0.0, 0.0),
        SphericalLocation.of(29.999993498010078, 59.99999597917299));
    List<SphericalLocation> actuals = service.getSphericalLocations(locations, partition);
    assertEquals(expecteds, actuals);
  }

  @Test
  public void getsMinecraftLocationOfSphericalLocation() {
    SphericalLocation location = SphericalLocation.of(30.0, 60.0);
    BlockLocation expected = BlockLocation.of(3339584.723798207, -6679169.447596414);
    BlockLocation actual = service.getMinecraftLocation(location, partition);
    assertEquals(expected, actual);
  }

  @Test
  public void getsMinecraftLocationsOfSphericalLocations() {
    List<SphericalLocation> locations = ImmutableList.of(
        SphericalLocation.of(0.0, 0.0),
        SphericalLocation.of(30.0, 60.0));
    List<BlockLocation> expecteds = ImmutableList.of(
        BlockLocation.of(0.0, -0.0),
        BlockLocation.of(3339584.723798207, -6679169.447596414));
    List<BlockLocation> actuals = service.getMinecraftLocations(locations, partition);
    assertEquals(expecteds, actuals);
  }

}
