// Copyright (C) 2024 Gerald Fiedler <gerald@ixilon.de>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package net.openstreetcraft.minecraft.world;

import java.util.Objects;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.openstreetcraft.coordinates.BlockLocation;
import net.openstreetcraft.coordinates.BoundingBox;
import net.openstreetcraft.projection.ProjectedLocation;
import net.openstreetcraft.tms.model.Tile;

/**
 * A partition is an area in the World where the difference between the highest and the lowest
 * altitude is smaller then the limits of the Minecraft height values.
 */
@Data
@Setter(AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Partition {
  
  private final World world = World.EARTH;
  private String name;
  private ProjectedLocation origin;
  private BoundingBox<BlockLocation> boundingBox;
  private double minHeight;
  private double maxHeight;
  
  Partition(Tile tile, BoundingBox<ProjectedLocation> bounds, double minHeight, double maxHeight) {
    origin = bounds.collect(Partition::center);
    this.boundingBox = bounds.forEach(this::localLocation);
    this.minHeight = minHeight;
    this.maxHeight = maxHeight;
    this.name = String.format("%s-%d-%d-%d", world.name(), tile.getZ(), tile.getX(), tile.getY());
  }
  
  private static ProjectedLocation center(BoundingBox<ProjectedLocation> bounds) {
    return ProjectedLocation.of(
        (bounds.getSo().getX() + bounds.getNw().getX()) / 2,
        (bounds.getNw().getY() + bounds.getSo().getY()) / 2);
  }
  
  /**
   * Convert between local and global coordinates.
   * 
   * @param location partition local coordinates
   * @return global coordinates
   */
  public ProjectedLocation globalLocation(BlockLocation location) {
    ProjectedLocation projected = ProjectedLocation.of(location);
    return ProjectedLocation.of(
        projected.getX() + origin.getX(),
        projected.getY() + origin.getY());
  }
  
  /**
   * Convert between local and global coordinates.
   * 
   * @param location global coordinates
   * @return partition local coordinates
   */
  public BlockLocation localLocation(ProjectedLocation location) {
    return BlockLocation.of(ProjectedLocation.of(
        location.getX() - origin.getX(),
        location.getY() - origin.getY()));
  }

  @Override
  public int hashCode() {
    return Objects.hash(name);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (!(obj instanceof Partition)) {
      return false;
    }
    Partition other = (Partition) obj;
    return Objects.equals(name, other.name);
  }

}
